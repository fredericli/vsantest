package com.cht.vsan.ves.service;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class InsFileRes {

    private String msg;
    private String uuidNumber;
}
