package com.cht.vsan.ves.service;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import lombok.SneakyThrows;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.List;

public class RestTemplateHelper {

    private static final Logger log = LoggerFactory.getLogger(RestTemplateHelper.class);

    private RestTemplate restTemplate;
    private ObjectMapper objectMapper;

    @Autowired
    public RestTemplateHelper(RestTemplate restTemplate, ObjectMapper objectMapper) {

        this.restTemplate = restTemplate;
        this.objectMapper = objectMapper;
    }

    @SneakyThrows
    public <T> T getForEntity(Class<T> clazz, String url, Object... uriVariables) {

        ResponseEntity<String> response = restTemplate.getForEntity(url, String.class,
                uriVariables);
        JavaType javaType = objectMapper.getTypeFactory().constructType(clazz);
        return readValue(response, javaType);
    }

    @SneakyThrows
    public <T> List<T> getForList(Class<T> clazz, String url, Object... uriVariables) {

        ResponseEntity<String> response = restTemplate.getForEntity(url, String.class,
                uriVariables);
        CollectionType collectionType = objectMapper.getTypeFactory()
                .constructCollectionType(List.class, clazz);
        return readValue(response, collectionType);

    }

    @SneakyThrows
    public <T, R> T postForEntity(Class<T> clazz, String url, R body, Object... uriVariables) {
        HttpEntity<R> request = new HttpEntity<>(body);
        ResponseEntity<String> response = restTemplate.postForEntity(url, request, String.class,
                uriVariables);
        JavaType javaType = objectMapper.getTypeFactory().constructType(clazz);
        return readValue(response, javaType);
    }

    @SneakyThrows
    public <T, R> T putForEntity(Class<T> clazz, String url, R body, Object... uriVariables) {
        HttpEntity<R> request = new HttpEntity<>(body);
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.PUT, request,
                String.class, uriVariables);
        JavaType javaType = objectMapper.getTypeFactory().constructType(clazz);
        return readValue(response, javaType);
    }

    @SneakyThrows
    public void delete(String url, Object... uriVariables) {

        restTemplate.delete(url, uriVariables);
    }

    private <T> T readValue(ResponseEntity<String> response, JavaType javaType) throws Exception {

        T result = null;
        if (response.getStatusCode() == HttpStatus.OK
                || response.getStatusCode() == HttpStatus.CREATED) {
            try {

                result = objectMapper.readValue(response.getBody(), javaType);
            } catch (Exception e) {

                log.error(ExceptionUtils.getStackTrace(e));
                throw e;
            }
        } else
            log.error("No data found {}", response.getStatusCode());

        return result;
    }
}
