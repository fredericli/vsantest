package com.cht.vsan.ves.service;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class VesFileReq {
    private String token;
    private String bucketId;
    private String uuidNumber;
}
