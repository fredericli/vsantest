package com.cht.vsan.ves.service;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class InsFile {
    private String token;
    private String bucketId;
    private String insnoKey;
    private String pdfdata;
    private String comment;
}

