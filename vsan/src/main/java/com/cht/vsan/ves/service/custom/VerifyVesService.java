package com.cht.vsan.ves.service.custom;

import com.cht.vsan.service.VsanQueryService;
import com.cht.vsan.service.VsanService;
import com.cht.vsan.service.dto.VsanCriteria;
import com.cht.vsan.service.dto.VsanDTO;
import com.cht.vsan.type.StatusType;
import com.cht.vsan.utils.FileUtils;
import com.cht.vsan.utils.HashUtils;
import com.cht.vsan.utils.StringFilterUtils;
import com.cht.vsan.ves.service.VesFileRes;
import com.cht.vsan.ves.service.VesWorkService;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.List;

@Slf4j
@Service
public class VerifyVesService {

    @Autowired
    private VsanQueryService vsanQueryService;

    @Autowired
    private VsanService vsanService;

    @Autowired
    private VesWorkService vesWorkService;

    private final String VVSAN_PATH = "/DATA/VVSan";

    @SneakyThrows
    public void doWork() {

        log.info("[開始]進行VES驗證任務:{}", System.currentTimeMillis());

        VsanCriteria criteria = new VsanCriteria();
        criteria.setStatus(StringFilterUtils.toEqualStringFilter(StatusType.VES_W_OK.toString()));


        List<VsanDTO> vsanDTOs = this.vsanQueryService.findByCriteria(criteria);

        log.info("\t 共計要去處理的資料筆數:{}", vsanDTOs.size());

        for (VsanDTO tmp : vsanDTOs) {
            try {
                String filePath = this.downloadFile(tmp);
                String fileHash = HashUtils.fileToHash(new File(filePath));
                if (fileHash.equals(tmp.getFileHash())) {
                    tmp.setStatus(StatusType.VES_V1_OK.toString());
                } else {
                    tmp.setStatus(StatusType.VES_V1_N_OK.toString());
                }
            } catch (Exception ex) {
                log.error(ExceptionUtils.getStackTrace(ex));
                tmp.setStatus(StatusType.VES_V1_EXCEPTION.toString());
            }
            this.vsanService.save(tmp);
        }

        log.info("[結束]進行VES驗證任務:{}", System.currentTimeMillis());


    }


    private String downloadFile(VsanDTO vsanDTO) throws IOException {


        String dir = String.valueOf(vsanDTO.getDir());

        VesFileRes vesFileRes = this.vesWorkService.getVesFile(dir, vsanDTO.getVesUuid());

        String filePath = VVSAN_PATH + File.separator + dir + File.separator + vsanDTO.getVesUuid();

        FileUtils.base64ToFile(vesFileRes.getPdfdata(), filePath);

        return filePath;
    }


}
