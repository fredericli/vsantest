package com.cht.vsan.ves.service;

import com.cht.vsan.service.VsanService;
import com.cht.vsan.service.dto.VsanDTO;
import com.cht.vsan.type.StatusType;
import com.cht.vsan.utils.JsonUtils;
import com.cht.vsan.utils.VesBcRestUtils;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
public class VesWorkService {

    @Value("${lia.vesBc.vesExe.vesToken}")
    private String VES_TOKEN;

    @Value("${lia.vesBc.vesExe.url}")
    private String VES_HOST_URL;

    @Autowired
    private VsanService vsanService;

    private static Map<String, Boolean> bucketIds = new HashMap<>();


    @SneakyThrows
    public synchronized void createBuckets(String bucketId) throws Exception {

        if (bucketIds.get(bucketId) != null) {
            return;
        }

        Bucket bucket = new Bucket();
        bucket.setToken(VES_TOKEN);
        bucket.setBucketId(bucketId);
        bucket.setBucketName(bucketId);

        String url = VES_HOST_URL + "/api/Bucket";

        BucketRes response = this.putForEntity(BucketRes.class, url, bucket);
        if ("true".equalsIgnoreCase(response.getMsg())) {
            bucketIds.put(bucketId, Boolean.TRUE);
        } else {
            log.error("建立Bucket任務:{},{}", bucketId, response);
        }
        log.info("建立Bucket任務:{},{}", bucketId, response);


    }

    public void putFile(String bucketId, VsanDTO vsanDTO) throws Exception {

        InsFile insFile = new InsFile();
        insFile.setBucketId(bucketId);
        insFile.setComment("VES PT");
        insFile.setInsnoKey(vsanDTO.getFileUuid());
        insFile.setPdfdata(this.fileToBase64(vsanDTO.getFilepath()));
        insFile.setToken(VES_TOKEN);


        String url = VES_HOST_URL + "/api/insurance";
        long l1 = System.currentTimeMillis();
        try {
            InsFileRes insFileRes = this.putForEntity(InsFileRes.class, url, insFile);
            log.info("建立檔案任務:{},{}", vsanDTO.getFileUuid(), insFileRes);
            if ("success".equalsIgnoreCase(insFileRes.getMsg())) {
                vsanDTO.setVesUuid(insFileRes.getUuidNumber());
                vsanDTO.setStatus(StatusType.VES_W_OK.toString());
            } else {
                log.error("發生錯誤:BucketId:{}, 檔案代碼:{}, 錯誤訊息:{}",vsanDTO.getDir(), vsanDTO.getFileUuid(), vsanDTO);
                vsanDTO.setStatus(StatusType.VES_W_N_OK.toString());
            }
        } catch (Exception ex) {
            log.error("發生錯誤:BucketId:{}, 檔案代碼:{}, 錯誤訊息:{}",vsanDTO.getDir(), vsanDTO.getFileUuid(), ex.getMessage());
            vsanDTO.setStatus(StatusType.VES_W_N_EXCEPTION.toString());
        }
//        InsFileRes response = new RestTemplate().postForEntity(url, insFile, InsFileRes.class).getBody();
        vsanDTO.setVesrr(Integer.valueOf(String.valueOf(System.currentTimeMillis() - l1)));
        this.vsanService.save(vsanDTO);


    }

    public VesFileRes getVesFile(String bucketId, String uuidNumber) throws IOException {


        VesFileReq vesFileObject = VesFileReq.builder()
            .token(VES_TOKEN)
            .bucketId(bucketId)
            .uuidNumber(uuidNumber)
            .build();

        String url = VES_HOST_URL + "/api/insurance";

        String result = VesBcRestUtils.get(url, vesFileObject);

        VesFileRes vesFileRes = JsonUtils.toObject(VesFileRes.class, result);

        return vesFileRes;

    }

    private String fileToBase64(final String path) throws IOException {
        final byte[] input_file = Files.readAllBytes(Paths.get(path));
        final byte[] encodedBytes = Base64.getEncoder().encode(input_file);
        return new String(encodedBytes);
    }

    public <T, R> T putForEntity(Class<T> clazz, String url, R body, Object... uriVariables) throws Exception {
        HttpEntity<R> request = new HttpEntity<>(body);
        ResponseEntity<String> response = new RestTemplate().exchange(url, HttpMethod.PUT, request,
            String.class, uriVariables);
        JavaType javaType = new ObjectMapper().getTypeFactory().constructType(clazz);
        return readValue(response, javaType);
    }

    private <T> T readValue(ResponseEntity<String> response, JavaType javaType) throws Exception {

        T result = null;
        if (response.getStatusCode() == HttpStatus.OK
            || response.getStatusCode() == HttpStatus.CREATED) {
            try {

                result = new ObjectMapper().readValue(response.getBody(), javaType);
            } catch (Exception e) {

                log.error(ExceptionUtils.getStackTrace(e));
                throw e;
            }
        } else
            log.error("No data found {}", response.getStatusCode());

        return result;
    }

    /** * api 取回檔案
     * @param host
     * @param uuidNumber
     * @param req
     * @return
     */
    // @Synchronized
//    @SneakyThrows
//    private String getVesFile(String host, String uuidNumber, LiaInsuranceVesBc req) {
//
//        log.info("Ves 主機：{}，取回 檔案 中 , fileUid：{}, uuidNumber：{}", host, req.getFileUid(), uuidNumber);
//
//        VesFileReq vesFileObject = VesFileReq.builder()
//            .token(this.VES_TOKEN)
//            .bucketId(req.getBucketId())
//            .uuidNumber(uuidNumber)
//            .build();
//
//        String result = VesBcRestUtils.get(
//            this.FILE_VERIFY_URL.replace("{host}", host)
//            , vesFileObject
//        );
//
//        log.info("Ves 主機：{}，取回 檔案 結果：{}", host, result.length()>128 ? result.substring(0, 128)+"…":result);
//
//        return result;
//    }


//    /**
//     * 工作流程
//     * * 依序打上去 開桶
//     * * 保單存證(上傳 檔案內容：由 原表的檔案資訊)
//     * * 取得 uuid1、2
//     * * 儲存 工作
//     *
//     * @param req
//     * @return
//     */
//    // @SneakyThrows
//    public LiaInsuranceVesBc workLogicVes1(LiaInsuranceVesBc req) {
//
//        log.info("=== 開始處理保單檔案 fileUid：{} 到 Ves1:{}", req.getFileUid(), this.VES_HOST_1);
//
//        try {
//
//            if (ObjectUtils.isEmpty(req.getVesBeginTime1())) req.setVesBeginTime1(Instant.now());
//
//            // ! uuid 1
//            if (StringUtils.isBlank(req.getUuidNumber1())) {
//                String uuid = this.getUuidNumber(this.VES_HOST_1, req);
//                if (StringUtils.isNotBlank(uuid)) {
//                    req.setUuidNumber1(uuid);
//                    req.setVesUploadTime1(Instant.now());
//                }
//            }
//
//        } catch (Exception e) {
//            this.exceptionProcess(e, req);
//        } finally {
//
//            Long frequency = req.getVesProcessFrequency1();
//            if (ObjectUtils.isNotEmpty(frequency)) {
//                req.setVesProcessFrequency1(Long.valueOf(frequency.longValue() + 1));
//            }
//            this.vesBcRepositoryC.save(req);
//        }
//
//        log.info("=== 結束處理保單檔案 fileUid：{}, uuid1:{}", req.getFileUid(), req.getUuidNumber1());
//
//        return req;
//    }
//
//    /**
//     * 工作流程
//     * * 依序打上去 開桶
//     * * 保單存證(上傳 檔案內容：由 原表的檔案資訊)
//     * * 取得 uuid1、2
//     * * 儲存 工作
//     *
//     * @param req
//     * @return
//     */
//    // @SneakyThrows
//    public LiaInsuranceVesBc workLogicVes2(LiaInsuranceVesBc req) {
//
//        log.info("=== 開始處理保單檔案 fileUid：{} 到 Ves2:{}", req.getFileUid(), this.VES_HOST_2);
//
//        try {
//
//            if (ObjectUtils.isEmpty(req.getVesBeginTime2())) req.setVesBeginTime2(Instant.now());
//
//            // ! uuid 2
//            if (StringUtils.isBlank(req.getUuidNumber2())) {
//                String uuid = this.getUuidNumber(this.VES_HOST_2, req);
//                if (StringUtils.isNotBlank(uuid)) {
//                    req.setUuidNumber2(uuid);
//                    req.setVesUploadTime2(Instant.now());
//                }
//            }
//        } catch (Exception e) {
//            this.exceptionProcess(e, req);
//        } finally {
//
//            Long frequency = req.getVesProcessFrequency2();
//            if (ObjectUtils.isNotEmpty(frequency)) {
//                req.setVesProcessFrequency2(Long.valueOf(frequency.longValue() + 1));
//            }
//            this.vesBcRepositoryC.save(req);
//        }
//
//        log.info("=== 結束處理保單檔案 fileUid：{}, uuid2:{}", req.getFileUid(), req.getUuidNumber2());
//
//        return req;
//    }
//
//    /**
//     * 處理 reference 保單
//     *
//     * @param req
//     * @return
//     */
//    public LiaInsuranceVesBc workRefInsNoVes1(LiaInsuranceVesBc req) {
//
//        log.info("=== 處理 RefInsNo1 保單檔案 Ins：{} Ref To {}", req.getInsNo(), req.getRefInsNo());
//
//        LiaInsuranceVesBc refLiaInsuranceVesBc = vesBcRepositoryC.findFirstByCompanyIdAndJobIdAndInsNo(req.getCompanyId(), req.getJobId(), req.getRefInsNo());
//
//        if (StringUtils.isNotBlank(refLiaInsuranceVesBc.getUuidNumber1())) {
//
//            try {
//
//                if (ObjectUtils.isEmpty(req.getVesBeginTime1())) req.setVesBeginTime1(Instant.now());
//
//                req.setUuidNumber1(refLiaInsuranceVesBc.getUuidNumber1());
//            } catch (Exception e) {
//
//                this.exceptionProcess(e, req);
//            } finally {
//
//                Long frequency = req.getVesProcessFrequency1();
//                if (ObjectUtils.isNotEmpty(frequency)) {
//                    req.setVesProcessFrequency1(Long.valueOf(frequency.longValue() + 1));
//                }
//
//                this.vesBcRepositoryC.save(req);
//            }
//        }
//
//        return req;
//    }
//
//    /**
//     * 處理 reference 保單
//     *
//     * @param req
//     * @return
//     */
//    public LiaInsuranceVesBc workRefInsNoVes2(LiaInsuranceVesBc req) {
//
//        log.info("=== 處理 RefInsNo2 保單檔案 Ins：{} Ref To {}", req.getInsNo(), req.getRefInsNo());
//
//        LiaInsuranceVesBc refLiaInsuranceVesBc = vesBcRepositoryC.findFirstByCompanyIdAndJobIdAndInsNo(req.getCompanyId(), req.getJobId(), req.getRefInsNo());
//
//        if (StringUtils.isNotBlank(refLiaInsuranceVesBc.getUuidNumber2())) {
//
//            try {
//
//                if (ObjectUtils.isEmpty(req.getVesBeginTime2())) req.setVesBeginTime2(Instant.now());
//
//                req.setUuidNumber2(refLiaInsuranceVesBc.getUuidNumber2());
//            } catch (Exception e) {
//
//                this.exceptionProcess(e, req);
//            } finally {
//
//                Long frequency = req.getVesProcessFrequency2();
//                if (ObjectUtils.isNotEmpty(frequency)) {
//                    req.setVesProcessFrequency2(Long.valueOf(frequency.longValue() + 1));
//                }
//
//                this.vesBcRepositoryC.save(req);
//            }
//        }
//
//        return req;
//    }
//
//    private void exceptionProcess(Exception e, LiaInsuranceVesBc req) {
//
//        if (e instanceof java.nio.file.NoSuchFileException)
//            log.error("找不到 保單檔案: {} ", req);
//        else
//            log.error("預期外的錯誤:");
//
//        log.error(ExceptionUtils.getStackTrace(e));
//    }
//
//    /**
//     * 檔案上傳，取得 Ves唯一碼(UuidNumber) 並 驗證結果
//     *
//     * @param host Ves server
//     * @param req
//     * @return Ves唯一碼(UuidNumber)
//     */
//    @SneakyThrows
//    private String getUuidNumber(String host, LiaInsuranceVesBc req) {
//
//        log.info("開始 put to Ves 主機：{}", host);
//
//        String result = this.putInsFile(host, req);
//
//        log.info("結束 put to Ves 主機：{}，uuidNumber：{}", host, result);
//
//        String uuid = Pattern.compile(this.PATTERN_UUID_NUMBER).matcher(result).matches() ? result : null;
//
//        return uuid;
//    }
//
//    /**
//     * api 開桶
//     *
//     * @param host
//     * @param bucketId
//     * @param bucketName
//     * @return
//     */
//    @SneakyThrows
//    private String putBucket(String host, String bucketId, String bucketName) {
//        log.info("Ves 主機：{}，桶 Id：{}", host, bucketId);
//
//        Bucket bucketObject = Bucket.builder()
//            .token(this.VES_TOKEN)
//            .bucketId(bucketId)
//            .bucketName(bucketName)
//            .build();
//
//        String result = VesBcRestUtils.curl(
//            this.UPDATE_BUCKET_URL.replace("{host}", host)
//            , HttpMethod.PUT
//            , bucketObject
//        );
//
//        return result;
//    }
//
//    /**
//     * api 保單存證
//     *
//     * @param job
//     * @return
//     */
//    @SneakyThrows
//    private String putInsFile(String host, LiaInsuranceVesBc job) {
//        InsFile insFileObject = InsFile.builder()
//            .token(this.VES_TOKEN)
//            .bucketId(job.getCompanyId())
//            .insnoKey(job.getCompanyId() + "_" + job.getInsNo())
//            .pdfdata(this.vesToFileContent(job))
//            .comment(job.getFileUid())
//            .build();
//
//        InsFileRes result = restTemplateHelper.putForEntity(InsFileRes.class, this.UPDATE_INS_FILE_URL.replace("{host}", host), insFileObject);
//
//        return result.getUuidNumber();
//    }
//
//    /**
//     * 取得檔案內容 by fileUuid
//     *
//     * @param job
//     * @return
//     */
//    @SneakyThrows
//    private String vesToFileContent(LiaInsuranceVesBc job) {
//
//        LiaInsurance file = this.liaInsuranceRepositoryC.findFirstByCompanyIdAndJobIdAndIndex(job.getCompanyId(), job.getJobId(), job.getIndex());
//
//        String filePath = file.getFilePath() + file.getFileName();
//
//        byte[] content = Files.readAllBytes(Paths.get(filePath));
//
//        return Base64.getUrlEncoder().encodeToString(content);
//    }
}
