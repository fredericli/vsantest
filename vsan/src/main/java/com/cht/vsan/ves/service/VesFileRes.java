package com.cht.vsan.ves.service;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class VesFileRes {
    private String msg;
    private String pdfdata;
    private String comment;
}
