package com.cht.vsan.ves.service.custom;

import com.cht.vsan.repository.custom.CustomVsanRepository;
import com.cht.vsan.service.MailService;
import com.cht.vsan.service.VSanTestService;
import com.cht.vsan.service.VsanService;
import com.cht.vsan.ves.service.VesWorkService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Component
//@ConditionalOnExpression("'${WriteFileSch}'.equals('true')")
public class FileWriteService {

    private static final Logger log = LoggerFactory.getLogger(FileWriteService.class);

    @Value("${waite_write:false}")
    public boolean isWaitWrite;

    @Value("${server.port}")
    public String port;


    @Value("${write.multuthread.number:50}")
    public Integer multiThreadNumber;

    @Autowired
    private VsanService vsanService;

    @Autowired
    private MailService mailService;

    @Autowired
    private VSanTestService vSanTestService;

    @Autowired
    private CustomVsanRepository vsanRepository;

    @Autowired
    private VesWorkService vesWorkService;

    private boolean isNext() throws InterruptedException {
        if (this.isWaitWrite) {
            if (this.vSanTestService.isBiggerThanMaxSpaceSize()) {
                log.error(
                    "[{}]已到達檔案上限，停止寫入30秒鐘，檔案上限是:{}, 資料庫容量是:{}",
                    port,
                    this.vSanTestService.max_sapce_size,
                    this.vSanTestService.getDbDataSize());
                this.vSanTestService.setTurnOff();
                Thread.sleep(1000 * 30);
                return false;
            } else {
                if (this.vSanTestService.isStop()) {
                    log.error(
                        "[{}]重新啟動寫入機制，檔案上限是:{}, 資料庫容量是:{}",
                        port,
                        this.vSanTestService.max_sapce_size,
                        this.vSanTestService.getDbDataSize());
                }
                this.vSanTestService.setTurnOn();
            }
        }
        return true;
    }

    //    @Scheduled(fixedDelayString = "5000")
    public void doWork() throws InterruptedException {

        log.info("啟動寫入時間序:{}", System.currentTimeMillis());

        ExecutorService pool = Executors.newFixedThreadPool(multiThreadNumber);

        int i = 0;
        while (true) {

            if (this.isNext()) {
                if (i % 3 == 0) {
                    pool.submit(new WriteFileThread(1, this.vsanService, this.vSanTestService, this.vesWorkService));
                } else if (i % 3 == 1) {
                    pool.submit(new WriteFileThread(2, this.vsanService, this.vSanTestService, this.vesWorkService));
                } else {
                    pool.submit(new WriteFileThread(3, this.vsanService, this.vSanTestService, this.vesWorkService));
                }
            }

            Thread.sleep(10);
            i++;
            if (i > 1000) {
                i = 0;
            }
        }
    }
}
