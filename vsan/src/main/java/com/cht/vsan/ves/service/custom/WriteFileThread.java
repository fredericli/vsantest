package com.cht.vsan.ves.service.custom;

import com.cht.vsan.service.VSanTestService;
import com.cht.vsan.service.VsanService;
import com.cht.vsan.service.dto.VsanDTO;
import com.cht.vsan.type.ParamType;
import com.cht.vsan.type.StatusType;
import com.cht.vsan.utils.CreateFileUtils;
import com.cht.vsan.utils.HashUtils;
import com.cht.vsan.utils.UuidUtils;
import com.cht.vsan.ves.service.VesWorkService;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.Callable;

public class WriteFileThread implements Callable<String> {

    private static final Logger log = LoggerFactory.getLogger(WriteFileThread.class);

    private final VsanService vsanService;
    private final int mode;
    private final VSanTestService vSanTestService;
    private final VesWorkService vesWorkService;

    public WriteFileThread(int mode, VsanService vsanService, VSanTestService vSanTestService, VesWorkService vesWorkService) {
        this.vsanService = vsanService;
        this.mode = mode;
        this.vSanTestService = vSanTestService;
        this.vesWorkService = vesWorkService;
    }

    /**
     * 取得當前日期
     *
     * @param convertFormat 轉換格式
     * @return 當前日期
     */
    public String getDate(String convertFormat) {
        Date date = new Date();
        return new SimpleDateFormat(convertFormat).format(date);
    }

    @Override
    public String call() {

        try {

            if (this.vSanTestService.isStop()) {
                return "N";
            }

            long start = System.currentTimeMillis();

            // 每一分鐘產一個目錄出來
            String dir = this.getDate("yyyyMMddHHmm");
//            long time = System.currentTimeMillis() / (60000);
            StringBuilder strb = new StringBuilder();
            strb.append(ParamType.Directory.getPath())
                .append("/")
                .append(String.valueOf(dir))
                .append("/");

            File file = new File(strb.toString());
            if (!file.exists()) {
                file.mkdirs();

            }
            // 開始寫檔案
            String fileName = UuidUtils.randomWithDateAndId(10);
            strb.append(fileName);
            file = new File(strb.toString());
            boolean isSuccess = true;
            try {
                if (this.mode == 1) {
                    // 大檔，20MB
                    CreateFileUtils.create(file);
                } else if (this.mode == 2) {
                    // 中檔
                    CreateFileUtils.create1(file);
                } else {
                    CreateFileUtils.creates(file);
                }
            } catch (Exception exn) {
                log.error(ExceptionUtils.getStackTrace(exn));
                isSuccess = false;
            }

            VsanDTO vsanDTO = new VsanDTO();
            vsanDTO.setFileUuid(fileName);
            vsanDTO.setFilepath(strb.toString());
            vsanDTO.setDir(Long.valueOf(dir));
            vsanDTO.setRr(Long.valueOf(System.currentTimeMillis() - start).intValue());
            if (isSuccess) {
                vsanDTO.setFilesize(this.div(new File(strb.toString()).length(), 1024, 10));
                vsanDTO.setFileHash(HashUtils.fileToHash(file));
                vsanDTO.setStatus(StatusType.FILE_OK.toString());
            } else {
                vsanDTO.setStatus(StatusType.FILE_N_OK.toString());
            }
            vsanDTO = this.vsanService.save(vsanDTO);

            this.vesWorkService.createBuckets(dir);
            this.vesWorkService.putFile(dir, vsanDTO);

        } catch (Exception e) {
            log.error(ExceptionUtils.getStackTrace(e));
        }
        return "A";
    }

    public double div(double v1, double v2, int scale) {
        if (scale < 0) {
            throw new IllegalArgumentException("The scale must be a positive integer or zero");
        }
        BigDecimal b1 = new BigDecimal(Double.toString(v1));
        BigDecimal b2 = new BigDecimal(Double.toString(v2));
        return b1.divide(b2, scale, BigDecimal.ROUND_HALF_UP).doubleValue();
    }
}
