package com.cht.vsan.repository;

import com.cht.vsan.domain.Vsan;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Vsan entity.
 */
@SuppressWarnings("unused")
@Repository
public interface VsanRepository extends JpaRepository<Vsan, Long>, JpaSpecificationExecutor<Vsan> {
}
