package com.cht.vsan.repository.custom;

import com.cht.vsan.repository.VsanRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomVsanRepository extends VsanRepository {

    @Query(value = "select sum(v.filesize) from Vsan v where v.status='1'")
    Double countFileSize();

    @Query(value = "select sum(v.filesize) from Vsan v where v.status='1' and v.dir=:dir")
    Double countFileSizeByDir(@Param("dir") Integer dir);

    @Modifying
    @Query("update Vsan v  set v.status='2' where v.dir = :dir")
    public void delete(@Param("dir") Integer dir);
}
