package com.cht.vsan;

import com.cht.vsan.config.ApplicationProperties;
import com.cht.vsan.ves.service.custom.FileWriteService;
import com.cht.vsan.ves.service.custom.VerifyVesService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.core.env.Environment;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Slf4j
@EnableTransactionManagement
@SpringBootApplication
@EnableConfigurationProperties({LiquibaseProperties.class, ApplicationProperties.class})
public class VsanApp implements CommandLineRunner {

    private final Environment env;
    private final FileWriteService write20FileService;
    private final VerifyVesService verifyVesService;

    public VsanApp(Environment env, FileWriteService write20FileService, VerifyVesService verifyVesService) {
        this.write20FileService = write20FileService;
        this.env = env;
        this.verifyVesService = verifyVesService;
    }


    /**
     * Main method, used to run the application.
     *
     * @param args the command line arguments.
     */
    public static void main(String[] args) {
        SpringApplication.run(VsanApp.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        if (args == null || args.length == 0) {
            log.error("參數錯誤");
            System.exit(1);
        }


        if ("WRITE".equalsIgnoreCase(args[0])) {
            log.info("啟動檔案寫入功能");
            this.write20FileService.doWork();
        } else if ("VERIFY".equalsIgnoreCase(args[0])) {
            while (true) {
                this.verifyVesService.doWork();
                Thread.sleep(5000);
            }
        }


    }
}
