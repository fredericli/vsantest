package com.cht.vsan.type;

public enum StatusType {

    FILE_OK, FILE_N_OK,


    VES_W_OK, VES_W_N_OK, VES_W_N_EXCEPTION,

    VES_V1_OK, VES_V1_N_OK, VES_V1_EXCEPTION,


    VES_V2_OK;
}
