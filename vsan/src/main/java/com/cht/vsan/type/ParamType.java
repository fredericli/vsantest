package com.cht.vsan.type;

import jdk.nashorn.internal.objects.annotations.Getter;

public enum ParamType {
    Directory("/DATA/VSan");

    public String getPath() {
        return path;
    }

    final String path;

    ParamType(String path) {
        this.path = path;
    }
}
