package com.cht.vsan.scheduler;

import com.cht.vsan.repository.custom.CustomVsanRepository;
import com.cht.vsan.service.MailService;
import com.cht.vsan.service.VSanTestService;
import com.cht.vsan.service.VsanQueryService;
import com.cht.vsan.service.VsanService;
import com.cht.vsan.service.custom.CustomVSanService;
import com.cht.vsan.type.ParamType;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;

@Component
//@ConditionalOnExpression("'${DeleteFileSch}'.equals('true')")
public class DeleteFileSch {

    private static final Logger log = LoggerFactory.getLogger(DeleteFileSch.class);

    @Autowired private MailService mailService;

    @Autowired private VsanQueryService vsanQueryService;
    @Autowired private VsanService vsanService;
    @Autowired private CustomVSanService customVSanService;
    @Autowired private VSanTestService vSanTestService;
    @Autowired private CustomVsanRepository vsanRepository;

//    @Scheduled(fixedDelayString = "5000")
    public void sch() throws InterruptedException {

        Double value = this.vsanRepository.countFileSize();
        while (value != null) {
            long vv1 = value.longValue();
            if (vv1 < this.vSanTestService.max_sapce_size) {
                break;
            }
            log.error(
                    "S0-需要去刪除檔案，已經到達臨界值了(KB):{}, 檔案總大小是:{}",
                    this.vSanTestService.max_sapce_size,
                    vv1);
            try {
                this.doDeleteDir();
            } catch (Exception ex) {
                log.error(ExceptionUtils.getStackTrace(ex));
            }
            value = this.vsanRepository.countFileSize();
        }
    }

    private void doDeleteDir() throws IOException {
        File file = new File(ParamType.Directory.getPath());
        File[] files = file.listFiles();

        long l1 = System.currentTimeMillis();
        log.error("S1-現在要去計算哪一個是最小的目錄");
        int deldir = this.getSmaallDir();
        Double dirValue = this.vsanRepository.countFileSizeByDir(deldir);
        if (dirValue == null) {
            // 代表這個是程式出錯，所以要把檔案也清掉
            log.error("S1-1例外狀況:{}", deldir);
            this.doDelete(deldir);
            return;
        }
        long dirlong = dirValue.longValue();
        log.error(
                "S1-完成去計算哪一個是最小的目錄:{}, 資料夾大小是(KB From DB):{}, 花費時間:{}",
                deldir,
                dirlong,
                System.currentTimeMillis() - l1);
        if (999999999 == deldir) {
            return;
        }

        this.doDelete(deldir);
    }

    private void doDelete(int deldir) throws IOException {
        StringBuilder strb = new StringBuilder();
        strb.append(ParamType.Directory.getPath())
                .append("/")
                .append(Integer.valueOf(deldir).toString());
        long l1 = System.currentTimeMillis();
        log.error("S2-現在要去刪除的是:{}", strb.toString());
        FileUtils.deleteDirectory(new File(strb.toString()));
        this.customVSanService.delete(Integer.valueOf(deldir));
        log.error("S2-完成要去刪除的是:{}, 花費時間:{}", strb.toString(), System.currentTimeMillis() - l1);
    }

    private int getSmaallDir() {

        int min = 999999999;
        File file = new File(ParamType.Directory.getPath());
        File[] files = file.listFiles();
        for (File tmp : files) {
            try {
                if ("source".equalsIgnoreCase(tmp.getName())
                        || ".DS_STore".equalsIgnoreCase(tmp.getName())) {
                    continue;
                }
                if (Integer.valueOf(tmp.getName()) < min) {
                    min = Integer.valueOf(tmp.getName());
                }

            } catch (Exception ex) {
                log.error(ExceptionUtils.getStackTrace(ex));
            }
        }
        return min;
    }

    private int getDeleteBeforeTime() {
        // 現在的時間，變成分鐘
        long ls = System.currentTimeMillis() / 60000;
        // 刪除3分鐘前的資料
        return Long.valueOf(ls).intValue() - 2;
    }
}
