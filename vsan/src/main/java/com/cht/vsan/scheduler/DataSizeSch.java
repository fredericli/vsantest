package com.cht.vsan.scheduler;

import com.cht.vsan.repository.custom.CustomVsanRepository;
import com.cht.vsan.service.VSanTestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@ConditionalOnExpression("'${DataSizeSch}'.equals('true')")
public class DataSizeSch {

    private static final Logger log = LoggerFactory.getLogger(DataSizeSch.class);

    @Autowired private VSanTestService vSanTestService;
    @Autowired private CustomVsanRepository vsanRepository;

    @Scheduled(fixedDelayString = "1000")
    public void sch() throws InterruptedException {

        Double value = this.vsanRepository.countFileSize();

        if (value == null) {
            this.vSanTestService.setDbDataSize(Long.valueOf(0));
        } else {
            long vv1 = value.longValue();
            this.vSanTestService.setDbDataSize(vv1);
        }
    }
}
