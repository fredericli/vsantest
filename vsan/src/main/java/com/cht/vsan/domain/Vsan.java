package com.cht.vsan.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.time.Instant;

/**
 * A Vsan.
 */
@Entity
@Table(name = "vsan")
public class Vsan implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "filepath")
    private String filepath;

    @Column(name = "dir")
    private Long dir;

    @Column(name = "filesize")
    private Double filesize;

    @Column(name = "status")
    private String status;

    @Column(name = "file_hash")
    private String fileHash;

    @Column(name = "file_uuid")
    private String fileUuid;

    @Column(name = "ves_uuid")
    private String vesUuid;

    @Column(name = "vv")
    private String vv;

    @Column(name = "vesrr")
    private Integer vesrr;

    @Column(name = "rr")
    private Integer rr;

    @Column(name = "create_date")
    private Instant createDate;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFilepath() {
        return filepath;
    }

    public Vsan filepath(String filepath) {
        this.filepath = filepath;
        return this;
    }

    public void setFilepath(String filepath) {
        this.filepath = filepath;
    }

    public Long getDir() {
        return dir;
    }

    public Vsan dir(Long dir) {
        this.dir = dir;
        return this;
    }

    public void setDir(Long dir) {
        this.dir = dir;
    }

    public Double getFilesize() {
        return filesize;
    }

    public Vsan filesize(Double filesize) {
        this.filesize = filesize;
        return this;
    }

    public void setFilesize(Double filesize) {
        this.filesize = filesize;
    }

    public String getStatus() {
        return status;
    }

    public Vsan status(String status) {
        this.status = status;
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFileHash() {
        return fileHash;
    }

    public Vsan fileHash(String fileHash) {
        this.fileHash = fileHash;
        return this;
    }

    public void setFileHash(String fileHash) {
        this.fileHash = fileHash;
    }

    public String getFileUuid() {
        return fileUuid;
    }

    public Vsan fileUuid(String fileUuid) {
        this.fileUuid = fileUuid;
        return this;
    }

    public void setFileUuid(String fileUuid) {
        this.fileUuid = fileUuid;
    }

    public String getVesUuid() {
        return vesUuid;
    }

    public Vsan vesUuid(String vesUuid) {
        this.vesUuid = vesUuid;
        return this;
    }

    public void setVesUuid(String vesUuid) {
        this.vesUuid = vesUuid;
    }

    public String getVv() {
        return vv;
    }

    public Vsan vv(String vv) {
        this.vv = vv;
        return this;
    }

    public void setVv(String vv) {
        this.vv = vv;
    }

    public Integer getVesrr() {
        return vesrr;
    }

    public Vsan vesrr(Integer vesrr) {
        this.vesrr = vesrr;
        return this;
    }

    public void setVesrr(Integer vesrr) {
        this.vesrr = vesrr;
    }

    public Integer getRr() {
        return rr;
    }

    public Vsan rr(Integer rr) {
        this.rr = rr;
        return this;
    }

    public void setRr(Integer rr) {
        this.rr = rr;
    }

    public Instant getCreateDate() {
        return createDate;
    }

    public Vsan createDate(Instant createDate) {
        this.createDate = createDate;
        return this;
    }

    public void setCreateDate(Instant createDate) {
        this.createDate = createDate;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Vsan)) {
            return false;
        }
        return id != null && id.equals(((Vsan) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Vsan{" +
            "id=" + getId() +
            ", filepath='" + getFilepath() + "'" +
            ", dir=" + getDir() +
            ", filesize=" + getFilesize() +
            ", status='" + getStatus() + "'" +
            ", fileHash='" + getFileHash() + "'" +
            ", fileUuid='" + getFileUuid() + "'" +
            ", vesUuid='" + getVesUuid() + "'" +
            ", vv='" + getVv() + "'" +
            ", vesrr=" + getVesrr() +
            ", rr=" + getRr() +
            ", createDate='" + getCreateDate() + "'" +
            "}";
    }
}
