package com.cht.vsan.service.custom;

import com.cht.vsan.repository.custom.CustomVsanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class CustomVSanService {

    @Autowired private CustomVsanRepository customVsanRepository;

    public void delete(Integer dir) {
        this.customVsanRepository.delete(dir);
    }
}
