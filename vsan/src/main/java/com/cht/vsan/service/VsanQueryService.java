package com.cht.vsan.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.cht.vsan.domain.Vsan;
import com.cht.vsan.domain.*; // for static metamodels
import com.cht.vsan.repository.VsanRepository;
import com.cht.vsan.service.dto.VsanCriteria;
import com.cht.vsan.service.dto.VsanDTO;
import com.cht.vsan.service.mapper.VsanMapper;

/**
 * Service for executing complex queries for {@link Vsan} entities in the database.
 * The main input is a {@link VsanCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link VsanDTO} or a {@link Page} of {@link VsanDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class VsanQueryService extends QueryService<Vsan> {

    private final Logger log = LoggerFactory.getLogger(VsanQueryService.class);

    private final VsanRepository vsanRepository;

    private final VsanMapper vsanMapper;

    public VsanQueryService(VsanRepository vsanRepository, VsanMapper vsanMapper) {
        this.vsanRepository = vsanRepository;
        this.vsanMapper = vsanMapper;
    }

    /**
     * Return a {@link List} of {@link VsanDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<VsanDTO> findByCriteria(VsanCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Vsan> specification = createSpecification(criteria);
        return vsanMapper.toDto(vsanRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link VsanDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<VsanDTO> findByCriteria(VsanCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Vsan> specification = createSpecification(criteria);
        return vsanRepository.findAll(specification, page)
            .map(vsanMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(VsanCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Vsan> specification = createSpecification(criteria);
        return vsanRepository.count(specification);
    }

    /**
     * Function to convert {@link VsanCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Vsan> createSpecification(VsanCriteria criteria) {
        Specification<Vsan> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Vsan_.id));
            }
            if (criteria.getFilepath() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFilepath(), Vsan_.filepath));
            }
            if (criteria.getDir() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDir(), Vsan_.dir));
            }
            if (criteria.getFilesize() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getFilesize(), Vsan_.filesize));
            }
            if (criteria.getStatus() != null) {
                specification = specification.and(buildStringSpecification(criteria.getStatus(), Vsan_.status));
            }
            if (criteria.getFileHash() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFileHash(), Vsan_.fileHash));
            }
            if (criteria.getFileUuid() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFileUuid(), Vsan_.fileUuid));
            }
            if (criteria.getVesUuid() != null) {
                specification = specification.and(buildStringSpecification(criteria.getVesUuid(), Vsan_.vesUuid));
            }
            if (criteria.getVv() != null) {
                specification = specification.and(buildStringSpecification(criteria.getVv(), Vsan_.vv));
            }
            if (criteria.getVesrr() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getVesrr(), Vsan_.vesrr));
            }
            if (criteria.getRr() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getRr(), Vsan_.rr));
            }
            if (criteria.getCreateDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreateDate(), Vsan_.createDate));
            }
        }
        return specification;
    }
}
