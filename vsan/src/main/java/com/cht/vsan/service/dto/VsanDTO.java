package com.cht.vsan.service.dto;

import java.time.Instant;
import java.io.Serializable;

/**
 * A DTO for the {@link com.cht.vsan.domain.Vsan} entity.
 */
public class VsanDTO implements Serializable {
    
    private Long id;

    private String filepath;

    private Long dir;

    private Double filesize;

    private String status;

    private String fileHash;

    private String fileUuid;

    private String vesUuid;

    private String vv;

    private Integer vesrr;

    private Integer rr;

    private Instant createDate;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFilepath() {
        return filepath;
    }

    public void setFilepath(String filepath) {
        this.filepath = filepath;
    }

    public Long getDir() {
        return dir;
    }

    public void setDir(Long dir) {
        this.dir = dir;
    }

    public Double getFilesize() {
        return filesize;
    }

    public void setFilesize(Double filesize) {
        this.filesize = filesize;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFileHash() {
        return fileHash;
    }

    public void setFileHash(String fileHash) {
        this.fileHash = fileHash;
    }

    public String getFileUuid() {
        return fileUuid;
    }

    public void setFileUuid(String fileUuid) {
        this.fileUuid = fileUuid;
    }

    public String getVesUuid() {
        return vesUuid;
    }

    public void setVesUuid(String vesUuid) {
        this.vesUuid = vesUuid;
    }

    public String getVv() {
        return vv;
    }

    public void setVv(String vv) {
        this.vv = vv;
    }

    public Integer getVesrr() {
        return vesrr;
    }

    public void setVesrr(Integer vesrr) {
        this.vesrr = vesrr;
    }

    public Integer getRr() {
        return rr;
    }

    public void setRr(Integer rr) {
        this.rr = rr;
    }

    public Instant getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Instant createDate) {
        this.createDate = createDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof VsanDTO)) {
            return false;
        }

        return id != null && id.equals(((VsanDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "VsanDTO{" +
            "id=" + getId() +
            ", filepath='" + getFilepath() + "'" +
            ", dir=" + getDir() +
            ", filesize=" + getFilesize() +
            ", status='" + getStatus() + "'" +
            ", fileHash='" + getFileHash() + "'" +
            ", fileUuid='" + getFileUuid() + "'" +
            ", vesUuid='" + getVesUuid() + "'" +
            ", vv='" + getVv() + "'" +
            ", vesrr=" + getVesrr() +
            ", rr=" + getRr() +
            ", createDate='" + getCreateDate() + "'" +
            "}";
    }
}
