package com.cht.vsan.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link com.cht.vsan.domain.Vsan} entity. This class is used
 * in {@link com.cht.vsan.web.rest.VsanResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /vsans?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class VsanCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter filepath;

    private LongFilter dir;

    private DoubleFilter filesize;

    private StringFilter status;

    private StringFilter fileHash;

    private StringFilter fileUuid;

    private StringFilter vesUuid;

    private StringFilter vv;

    private IntegerFilter vesrr;

    private IntegerFilter rr;

    private InstantFilter createDate;

    public VsanCriteria() {
    }

    public VsanCriteria(VsanCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.filepath = other.filepath == null ? null : other.filepath.copy();
        this.dir = other.dir == null ? null : other.dir.copy();
        this.filesize = other.filesize == null ? null : other.filesize.copy();
        this.status = other.status == null ? null : other.status.copy();
        this.fileHash = other.fileHash == null ? null : other.fileHash.copy();
        this.fileUuid = other.fileUuid == null ? null : other.fileUuid.copy();
        this.vesUuid = other.vesUuid == null ? null : other.vesUuid.copy();
        this.vv = other.vv == null ? null : other.vv.copy();
        this.vesrr = other.vesrr == null ? null : other.vesrr.copy();
        this.rr = other.rr == null ? null : other.rr.copy();
        this.createDate = other.createDate == null ? null : other.createDate.copy();
    }

    @Override
    public VsanCriteria copy() {
        return new VsanCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getFilepath() {
        return filepath;
    }

    public void setFilepath(StringFilter filepath) {
        this.filepath = filepath;
    }

    public LongFilter getDir() {
        return dir;
    }

    public void setDir(LongFilter dir) {
        this.dir = dir;
    }

    public DoubleFilter getFilesize() {
        return filesize;
    }

    public void setFilesize(DoubleFilter filesize) {
        this.filesize = filesize;
    }

    public StringFilter getStatus() {
        return status;
    }

    public void setStatus(StringFilter status) {
        this.status = status;
    }

    public StringFilter getFileHash() {
        return fileHash;
    }

    public void setFileHash(StringFilter fileHash) {
        this.fileHash = fileHash;
    }

    public StringFilter getFileUuid() {
        return fileUuid;
    }

    public void setFileUuid(StringFilter fileUuid) {
        this.fileUuid = fileUuid;
    }

    public StringFilter getVesUuid() {
        return vesUuid;
    }

    public void setVesUuid(StringFilter vesUuid) {
        this.vesUuid = vesUuid;
    }

    public StringFilter getVv() {
        return vv;
    }

    public void setVv(StringFilter vv) {
        this.vv = vv;
    }

    public IntegerFilter getVesrr() {
        return vesrr;
    }

    public void setVesrr(IntegerFilter vesrr) {
        this.vesrr = vesrr;
    }

    public IntegerFilter getRr() {
        return rr;
    }

    public void setRr(IntegerFilter rr) {
        this.rr = rr;
    }

    public InstantFilter getCreateDate() {
        return createDate;
    }

    public void setCreateDate(InstantFilter createDate) {
        this.createDate = createDate;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final VsanCriteria that = (VsanCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(filepath, that.filepath) &&
            Objects.equals(dir, that.dir) &&
            Objects.equals(filesize, that.filesize) &&
            Objects.equals(status, that.status) &&
            Objects.equals(fileHash, that.fileHash) &&
            Objects.equals(fileUuid, that.fileUuid) &&
            Objects.equals(vesUuid, that.vesUuid) &&
            Objects.equals(vv, that.vv) &&
            Objects.equals(vesrr, that.vesrr) &&
            Objects.equals(rr, that.rr) &&
            Objects.equals(createDate, that.createDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        filepath,
        dir,
        filesize,
        status,
        fileHash,
        fileUuid,
        vesUuid,
        vv,
        vesrr,
        rr,
        createDate
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "VsanCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (filepath != null ? "filepath=" + filepath + ", " : "") +
                (dir != null ? "dir=" + dir + ", " : "") +
                (filesize != null ? "filesize=" + filesize + ", " : "") +
                (status != null ? "status=" + status + ", " : "") +
                (fileHash != null ? "fileHash=" + fileHash + ", " : "") +
                (fileUuid != null ? "fileUuid=" + fileUuid + ", " : "") +
                (vesUuid != null ? "vesUuid=" + vesUuid + ", " : "") +
                (vv != null ? "vv=" + vv + ", " : "") +
                (vesrr != null ? "vesrr=" + vesrr + ", " : "") +
                (rr != null ? "rr=" + rr + ", " : "") +
                (createDate != null ? "createDate=" + createDate + ", " : "") +
            "}";
    }

}
