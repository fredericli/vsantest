package com.cht.vsan.service;

import com.cht.vsan.domain.Vsan;
import com.cht.vsan.repository.VsanRepository;
import com.cht.vsan.service.dto.VsanDTO;
import com.cht.vsan.service.mapper.VsanMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Vsan}.
 */
@Service
@Transactional
public class VsanService {

    private final Logger log = LoggerFactory.getLogger(VsanService.class);

    private final VsanRepository vsanRepository;

    private final VsanMapper vsanMapper;

    public VsanService(VsanRepository vsanRepository, VsanMapper vsanMapper) {
        this.vsanRepository = vsanRepository;
        this.vsanMapper = vsanMapper;
    }

    /**
     * Save a vsan.
     *
     * @param vsanDTO the entity to save.
     * @return the persisted entity.
     */
    public VsanDTO save(VsanDTO vsanDTO) {
        log.debug("Request to save Vsan : {}", vsanDTO);
        Vsan vsan = vsanMapper.toEntity(vsanDTO);
        vsan = vsanRepository.save(vsan);
        return vsanMapper.toDto(vsan);
    }

    /**
     * Get all the vsans.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<VsanDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Vsans");
        return vsanRepository.findAll(pageable)
            .map(vsanMapper::toDto);
    }


    /**
     * Get one vsan by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<VsanDTO> findOne(Long id) {
        log.debug("Request to get Vsan : {}", id);
        return vsanRepository.findById(id)
            .map(vsanMapper::toDto);
    }

    /**
     * Delete the vsan by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Vsan : {}", id);
        vsanRepository.deleteById(id);
    }
}
