package com.cht.vsan.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class VSanTestService {

    private static final Logger log = LoggerFactory.getLogger(VSanTestService.class);

    // 單位 KB
    @Value("${max_sapce_size:104857600}")
    public long max_sapce_size;

    private boolean isStop = false;

    private Long dbDataSize = Long.valueOf(0);

    public synchronized void setTurnOff() {
        this.isStop = true;
    }

    public Long getDbDataSize() {
        return dbDataSize;
    }

    public synchronized void setDbDataSize(Long dbDataSize) {
        this.dbDataSize = dbDataSize;
    }

    public synchronized void setTurnOn() {
        this.isStop = false;
    }

    public boolean isStop() {
        return this.isStop;
    }

    public boolean isBiggerThanMaxSpaceSize() {
        if (this.dbDataSize > this.max_sapce_size) {
            return true;
        }
        return false;
    }
}
