package com.cht.vsan.service.mapper;


import com.cht.vsan.domain.*;
import com.cht.vsan.service.dto.VsanDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Vsan} and its DTO {@link VsanDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface VsanMapper extends EntityMapper<VsanDTO, Vsan> {



    default Vsan fromId(Long id) {
        if (id == null) {
            return null;
        }
        Vsan vsan = new Vsan();
        vsan.setId(id);
        return vsan;
    }
}
