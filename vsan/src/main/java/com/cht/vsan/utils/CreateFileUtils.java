package com.cht.vsan.utils;

import com.google.common.io.Resources;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Random;

public class CreateFileUtils {

    private static String getContent() throws IOException {

//        Files.readAllBytes

        URL url = Resources.getResource("mysource.txt");
        String text = Resources.toString(url, StandardCharsets.UTF_8);

//        return new String(
//                Files.readAllBytes(Paths.get("/DATA/VSan/source")), StandardCharsets.UTF_8);
        return text;
    }

    // 建立1GB的大檔案
    public static void create(File file) throws IOException {
        FileWriter fileWriter = new FileWriter(file);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        while (file.length() < (1024 * 1024 * 20)) {
            bufferedWriter.write(getContent());
        }
        bufferedWriter.close();
    }

    public static void create1(File file) throws IOException {
        FileWriter fileWriter = new FileWriter(file);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        while (file.length() < (1024 * 1024)) {
            bufferedWriter.write(createTemp());
        }
        bufferedWriter.close();
    }

    public static void creates(File file) throws IOException {
        FileWriter fileWriter = new FileWriter(file);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        bufferedWriter.write("1234567890");
        bufferedWriter.close();
    }

    // 建立1KB的字串
    public static String createTemp() {
        StringBuilder temp = new StringBuilder();
        while (temp.length() < 1024) {
            char random = createRandom(65, 122); // 通過Ascii碼來建立隨機字串，比從字串中擷取字串來構建高效
            temp.append(random);
        }
        return temp.toString();
    }

    // 建立min和max範圍內的隨機字元
    public static char createRandom(int min, int max) {
        Random random = new Random();
        int i = random.nextInt(max - min + 1) + min;
        char ch = (char) i;
        return ch;
    }

    public static void main(String[] args) throws IOException {
        long starttime = System.currentTimeMillis();
        File file = new File("D:\\a.txt");
        file.createNewFile();
        create(file);
        long endtime = System.currentTimeMillis();
        System.out.println("程式執行時間" + (endtime - starttime)); // 程式執行時間325737
    }
}
