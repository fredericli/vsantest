package com.cht.vsan.utils;

import com.google.common.hash.Hashing;
import com.google.common.io.Files;

import java.io.File;
import java.io.IOException;

public class HashUtils {

    public static String fileToHash(File sf) throws IOException {
        return Files.asByteSource(sf).hash(Hashing.sha512()).toString();
    }
}
