package com.cht.vsan.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;


public class ReadFileUtils {
    public static void read(File file) throws IOException {
        FileInputStream fileInputStream = new FileInputStream(file);
        byte[] bytes = new byte[1024 * 1024 * 1024];
        int length = 0;
        while ((length = fileInputStream.read(bytes)) != -1) {
            System.out.println(new String(bytes, 0, length));
        }
        fileInputStream.close();
    }

    public static void main(String[] args) throws IOException {
        long starttime = System.currentTimeMillis();
        File file = new File("D:\\a.txt");
        read(file);
        long endtime = System.currentTimeMillis();
        System.out.println(
                "執行時間為：" + (endtime - starttime)); // 緩衝陣列為1kb執行時間為：36440;緩衝陣列為1MB執行時間為：22641
        // 緩衝陣列為1GB報java.lang.OutOfMemoryError，因為預設的Java虛擬機器最大記憶體僅為64MB
        // 緩衝陣列為60MB執行時間為：30180
        // 緩衝陣列為30MB執行時間為：27764
    }
}
