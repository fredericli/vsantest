package com.cht.vsan.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

@Slf4j
public class FileUtils {

    public static String sepSymbol = SysSymbol.FILE_SEPARATOR.getCode();

    public static boolean mkdir(final String path) {
        if (path == null) return false;
        final File file = new File(path);
        if (!file.exists()) {
            return file.mkdirs();
        }
        return false;
    }

    public static String fileToBase64(final String path) throws IOException {
        final byte[] input_file = Files.readAllBytes(Paths.get(path));
        final byte[] encodedBytes = Base64.getEncoder().encode(input_file);
        return new String(encodedBytes);
    }

    public static String bytesToBase64(final byte[] fileBytes) throws IOException {
        final byte[] encodedBytes = Base64.getEncoder().encode(fileBytes);
        return new String(encodedBytes);
    }

    public static byte[] fileToBytes(final String path) throws IOException {
        return Files.readAllBytes(Paths.get(path));
    }

    public static void base64ToFile(final String base64, final String filePath) throws IOException {
        final byte[] output_file = decodeB64(base64);
        mkdir(new File(filePath).getParent());
        org.apache.commons.io.FileUtils.writeByteArrayToFile(new File(filePath), output_file);
    }

    /**
     * 寫入檔案
     *
     * @param content
     * @param filePath
     */
    public static void writeByteArrayToFile(final byte[] content, final String filePath) throws IOException {
        mkdir(new File(filePath).getParent());
        org.apache.commons.io.FileUtils.writeByteArrayToFile(new File(filePath), content);
    }


    /**
     * 取得 base64檔案大小
     *
     * @param base64 檔案base64格式
     * @return 回傳檔案大小 (單位 KB)
     */
    public static String base64FileSize(final String base64) {
        long padding;
        final int totalLength = base64.length();
        if (!StringUtils.isNotBlank(base64)) {
            return "0";
        }
        if (base64.substring(totalLength - 1).equals("=")) {
            if (base64.substring(totalLength - 2, totalLength - 1).equals("=")) {
                padding = 2;
            } else {
                padding = 1;
            }
        } else {
            padding = 0;
        }
        // 四捨五入到第二位
        final double size =
            Math.round(((((double) (totalLength) * (3.0 / 4.0)) - padding) / 1024) * 100)
                / 100.0;
        return String.valueOf(size);
    }

    /**
     * 解碼 Base64 內容
     *
     * @param strContent 編譯過的內容字串
     * @return 解碼的二進內容
     */
    public static byte[] decodeB64(final String strContent) {
        if (strContent.startsWith("data:application/octet-stream;base64,")) {
            return Base64.getDecoder().decode(strContent.split(",")[1]);
        } else {
            return Base64.getDecoder().decode(strContent);
        }
    }

    /**
     * 搬移檔案
     *
     * @param from 起始路徑
     * @param to   搬移路徑
     * @return 成功與否
     */
    public static boolean moveFile(final String from, final String to) {
        final File file = new File(from);
        final File targetFile = new File(to);
        if (!targetFile.getParentFile().exists()) {
            targetFile.getParentFile().mkdirs();
        }
        return file.renameTo(targetFile);
    }

    /**
     * 複製檔案
     *
     * @param from
     * @param to
     * @throws IOException
     */
//    public static void copy(final String from, final String to) throws Exception {
//        FileUtils.copy(from, to, true);
//    }

    /**
     * 複製檔案(with 檢查碼開關)
     *
     * @param from
     * @param to
     * @param hasCheck
     * @throws IOException
     */
//    public static void copy(final String from, final String to, final boolean hasCheck) throws Exception {
//        try {
//            final File source = new File(from);
//            if (!source.getParentFile().exists()) {
//                source.getParentFile().mkdirs();
//            }
//            final File destination = new File(to);
//            if (!destination.getParentFile().exists()) {
//                destination.getParentFile().mkdirs();
//            }
//            com.google.common.io.Files.copy(source, destination);
//            if (hasCheck) {
//                String fromHash = HashUtils.fileToHash(source.getPath());
//                String toHash = HashUtils.fileToHash(destination.getPath());
//                if (!fromHash.equals(toHash)
//                    || StringUtils.isBlank(fromHash)
//                    || StringUtils.isBlank(toHash)) {
//                    throw new IOException(
//                        "檔案搬移出現異常,fileHash比對錯誤,來源:"
//                            + source.getAbsolutePath()
//                            + ",目的檔案:"
//                            + destination.getAbsolutePath());
//                }
//            }
//        } catch (Exception e) {
//            log.error("檔案搬移出現異常:{}", ExceptionUtils.getStackTrace(e));
//            throw e;
//        }
//    }

    /**
     * 取得資料夾目錄下的全部檔案
     *
     * @param dirPath 資料夾位置
     * @return 裡面的檔案陣列
     */
    public static List<File> getDirFileList(final String dirPath) {
        final List<File> fileList = new ArrayList<>();
        final File dirFile = new File(dirPath);
        final String absolutePath = dirFile.getAbsolutePath(); // 取得絕對路徑
        if (dirFile.isDirectory()) {
            for (final String fileName : Objects.requireNonNull(dirFile.list())) {
                final File file = new File(absolutePath + sepSymbol + fileName);
                if (!file.isDirectory() && !file.getName().substring(0, 1).equals("."))
                    fileList.add(file);
            }
        } else {
            log.info(dirPath + "資料夾不存在");
        }
        return fileList;
    }

    /**
     * 取得資料夾目錄下的全部檔案(副檔名為指定)
     *
     * @param dirPath   資料夾位置
     * @param extension 副檔名格式
     * @return 裡面的檔案陣列
     */
    public static List<File> getDirFileList(final String dirPath, final String extension) {
        final List<File> fileList = new ArrayList<>();
        final File dirFile = new File(dirPath);
        final String absolutePath = dirFile.getAbsolutePath(); // 取得絕對路徑
        if (dirFile.isDirectory()) {
            for (final String fileName : Objects.requireNonNull(dirFile.list())) {
                final File file = new File(absolutePath + sepSymbol + fileName);
                if (!file.isDirectory()
                    && !file.getName().substring(0, 1).equals(".")
                    && FilenameUtils.getExtension(file.getName()).equals(extension))
                    fileList.add(file);
            }
        } else {
            log.info(dirPath + " 資料夾不存在");
        }
        return fileList;
    }

    /**
     * 檢查資料夾路徑最後是否有斜線
     *
     * @param dirPath 資料夾路徑
     * @return 處理後的路徑
     */
    public static String checkDirPath(final String dirPath) {
        if (dirPath.substring(dirPath.length() - 1).equals("\\")
            || dirPath.substring(dirPath.length() - 1).equals("/")) {
            return dirPath;
        } else return dirPath + sepSymbol;
    }

    /**
     * 檢查檔案是否存在
     *
     * @param path
     * @return
     */
    public static Boolean isExists(final String path) {
        final File tmp = new File(path);
        return tmp.exists();
    }

    public static String getSignSep() {
        final String os = System.getProperty("os.name").toLowerCase();
        if (os.indexOf("win") >= 0) {
            return "\\\\";
        }
        return System.getProperty("file.separator");
    }

    /**
     * 修正路徑位置
     *
     * @param path 路徑
     * @return 修正後路徑
     */
    public static String fixPath(final String path) {
        String fixPath = path.replace("/", sepSymbol);
        fixPath = fixPath.replace("\\", sepSymbol);
        return fixPath;
    }

    public static boolean checkUse(final String path) {
        final File file = new File(path);
        if (!file.exists()) return false;
        if (!file.canExecute()) return false;
        if (!file.canRead()) return false;
        return file.canWrite();
    }

    /**
     * 將base64轉成Byte array格式
     *
     * @param base64 檔案的base64
     * @return 檔案的byte[]
     */
    public static byte[] base64ToByteArray(final String base64) {
        return Base64.getDecoder().decode(base64.getBytes());
    }

    /**
     * 判斷data是否為pdf檔案
     *
     * @param data byte[]格式的檔案
     * @return 是pdf檔案回傳true, 不是則回傳false
     */
    public static boolean isPDF(final byte[] data) {
        if (data == null || data.length < 5) return false;
        // %PDF-
        if (data[0] == 0x25
            && data[1] == 0x50
            && data[2] == 0x44
            && data[3] == 0x46
            && data[4] == 0x2D) {
            int offset = data.length - 8,
                count = 0; // check last 8 bytes for %%EOF wiisPDFth optional
            // white-space
            boolean hasSpace = false, hasCr = false, hasLf = false;
            while (offset < data.length) {
                if (count == 0 && data[offset] == 0x25) count++; // %
                if (count == 1 && data[offset] == 0x25) count++; // %
                if (count == 2 && data[offset] == 0x45) count++; // E
                if (count == 3 && data[offset] == 0x4F) count++; // O
                if (count == 4 && data[offset] == 0x46) count++; // F
                // Optional flags for meta info
                if (count == 5 && data[offset] == 0x20) hasSpace = true; // SPACE
                if (count == 5 && data[offset] == 0x0D) hasCr = true; // CR
                if (count == 5 && data[offset] == 0x0A) hasLf = true; // LF / EOL
                offset++;
            }

            if (count == 5) {
                // String version = data.length > 13 ? String.format("%s%s%s", (char) data[5],
                // (char) data[6], (char) data[7]) : "?";
                return true;
            }
        }
        return false;
    }

    public static void outputTextFile(final String dir, final String fileName, final String saveData) {
        final File file = new File(checkDirPath(dir), fileName);
        FileOutputStream fileOutputStream;
        try {
            fileOutputStream = new FileOutputStream(file);
            final OutputStreamWriter oStreamWriter = new OutputStreamWriter(fileOutputStream, StandardCharsets.UTF_8);
            oStreamWriter.append(saveData);
            oStreamWriter.close();
        } catch (final IOException e) {
            log.error("[FileUtils]{}", ExceptionUtils.getStackTrace(e));
        }
    }

    /**
     * 檢查base64格式是否正確，若有誤則拋出exception
     *
     * @param base64 檔案base64格式
     * @throws IOException
     */
    public static void testBase64ToFile(final String base64) throws IOException {
        decodeB64(base64);
    }

    /**
     * 回傳 指定檔案
     *
     * @param filePath
     * @return
     * @throws Exception
     */
    public static File loadFile(final String filePath) throws Exception {
        final File f = new File(Optional.ofNullable(filePath).orElse(""));
        if (!f.exists()) {
            log.warn("檔案或目錄不存在！");
            mkdir(f.getParent());
            f.createNewFile();
            log.warn("建立檔案：" + filePath);
        } else if (!f.canRead() || !f.canWrite()) {
            throw new Exception("權限問題");
        }

        return f;
    }

    /**
     * 讀取 指定檔案
     *
     * @param file
     */
    public static List<String> readFile(final File file) {
        BufferedReader reader = null;
        final List<String> content = new ArrayList<>();
        try {
            String str = "";
            reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8")); // 指定讀取文件的編碼格式，以免出現中文亂碼
            while ((str = reader.readLine()) != null) {
                content.add(str);
            }
        } catch (final FileNotFoundException e) {
            log.error("[FileUtils]{}", ExceptionUtils.getStackTrace(e));
        } catch (final IOException e) {
            log.error("[FileUtils]{}", ExceptionUtils.getStackTrace(e));
        } finally {
            try {
                reader.close();
                content.add("");
            } catch (final IOException e) {
                log.error("[FileUtils]{}", ExceptionUtils.getStackTrace(e));
                log.error("無法關檔:" + file.getPath());
            }
            return content;
        }
    }

    /**
     * 寫入 指定檔案
     *
     * @param file
     * @param content 欲寫入的內容
     */
    public static void writeFile(final File file, final String content) {
        BufferedWriter fw = null;
        try {
            fw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file, false), "UTF-8")); // 指點編碼格式，以免讀取時中文字符異常
            fw.append(content);
            fw.flush(); // 全部寫入緩存中的內容
        } catch (final Exception e) {
            log.error("[FileUtils]{}", ExceptionUtils.getStackTrace(e));
            log.error("寫檔失敗:" + file.getPath());
        } finally {
            if (fw != null) {
                try {
                    fw.close();
                } catch (final IOException e) {
                    log.error("[FileUtils]{}", ExceptionUtils.getStackTrace(e));
                    log.error("無法關檔:" + file.getPath());
                }

            }
        }
    }

    /**
     * 刪檔
     *
     * @param target 檔案位置
     * @throws Exception
     */
    public static void delete(final String target) throws Exception {
        File file = new File(target);
        if (file.exists()) {
            file.delete();
        } else {
            log.warn("找不到要刪除的檔案:{}", target);
        }
    }
}
