package com.cht.vsan.utils;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.Arrays;
import java.util.Map;

@Slf4j
public class VesBcRestUtils {

    private static RestTemplate REST = new RestTemplate();

//    public static void setRestTemplate(RestTemplate restTemplate) {
//
//        REST = restTemplate;
//    }

    /* no head ================================================================================= */
    /**
     * default method GET
     * @param <E>
     * @param head
     * @param url
     * @return
     */
    @SneakyThrows
    public static <E> String get(String url) {

        return curl(null, url, null, null, null);
    }

    /**
     * default method GET
     * @param <E>
     * @param head
     * @param url
     * @param param
     * @return
     */
    @SneakyThrows
    public static <E> String get(String url, Object param) {

        return curl(null, url, null, null, param);
    }

    @SneakyThrows
    public static <E> String curl(String url, HttpMethod method) {

        return curl(null, url, method, null, null);
    }

    @SneakyThrows
    public static <E> String curl(String url, HttpMethod method, E dto) {

        return curl(null, url, method, dto, null);
    }

    @SneakyThrows
    public static <E> String curl(String url, HttpMethod method, E dto, Object param) {

        return curl(null, url, method, dto, param);
    }


    /* with head =============================================================================== */
    /**
     * default method GET
     * @param <E>
     * @param head
     * @param url
     * @return
     */
    @SneakyThrows
    public static <E> String get(Map<String, Object> head, String url) {

        return curl(head, url, null, null, null);
    }

    /**
     * default method GET
     * @param <E>
     * @param head
     * @param url
     * @param param
     * @return
     */
    @SneakyThrows
    public static <E> String get(Map<String, Object> head, String url, Object param) {

        return curl(head, url, null, null, param);
    }

    @SneakyThrows
    public static <E> String curl(Map<String, Object> head, String url, HttpMethod method) {

        return curl(head, url, method, null, null);
    }

    @SneakyThrows
    public static <E> String curl(Map<String, Object> head, String url, HttpMethod method, E dto) {

        return curl(head, url, method, dto, null);
    }

    @SneakyThrows
    public static <E> String curl(Map<String, Object> head, String url, HttpMethod method, E dto, Object param) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        if(head != null)
            head.entrySet().stream().forEach(map->headers.add(map.getKey(),map.getValue().toString()));

        HttpMethod httpMethod = method == null
            ? HttpMethod.GET
            : method
            ;

        String uri = param == null
            ? url
            : url + "?" + MapperUtils.toUrlEncoded(param)
            ;

        log.debug("curl：{} {} {} {} {}", headers, httpMethod, uri, dto, param);
        return REST.exchange(
            URI.create(uri)
            , httpMethod
            , new HttpEntity<>(dto, headers)
            , String.class
            )
            .getBody();
    }
}
