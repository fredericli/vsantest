package com.cht.vsan.utils;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class DateUtils {

    /**
     * 日期時區轉換器
     *
     * @param instant 日期
     * @param zoneId_1 來源時區
     * @param zoneId_2 目的時區
     * @param convertFormat 轉換格式
     * @return 轉換後字串
     */
    public static String convertZone(
            Instant instant, ZoneId zoneId_1, ZoneId zoneId_2, String convertFormat) {
        ZonedDateTime time1 = ZonedDateTime.ofInstant(instant, zoneId_1);
        ZonedDateTime time2 = time1.withZoneSameInstant(zoneId_2);
        return DateTimeFormatter.ofPattern(convertFormat).format(time2);
    }

    /**
     * 日期格式轉換器
     *
     * @param instant 日期
     * @param convertFormat 轉換格式
     * @return 轉換後字串
     */
    public static String convertDate(Instant instant, String convertFormat) {
        Date date = Date.from(instant);
        return new SimpleDateFormat(convertFormat).format(date);
    }

    /**
     * 日期格式轉換器
     *
     * @param date 日期
     * @param convertFormat 轉換格式
     * @return 轉換後字串
     */
    public static String convertDate(Date date, String convertFormat) {
        return new SimpleDateFormat(convertFormat).format(date);
    }

    /**
     * 取得當前日期
     *
     * @param convertFormat 轉換格式
     * @return 當前日期
     */
    public static String getDate(String convertFormat) {
        Date date = new Date();
        return new SimpleDateFormat(convertFormat).format(date);
    }
}
