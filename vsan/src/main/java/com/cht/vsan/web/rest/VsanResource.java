package com.cht.vsan.web.rest;

import com.cht.vsan.service.VsanService;
import com.cht.vsan.web.rest.errors.BadRequestAlertException;
import com.cht.vsan.service.dto.VsanDTO;
import com.cht.vsan.service.dto.VsanCriteria;
import com.cht.vsan.service.VsanQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.cht.vsan.domain.Vsan}.
 */
@RestController
@RequestMapping("/api")
public class VsanResource {

    private final Logger log = LoggerFactory.getLogger(VsanResource.class);

    private static final String ENTITY_NAME = "vsan";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final VsanService vsanService;

    private final VsanQueryService vsanQueryService;

    public VsanResource(VsanService vsanService, VsanQueryService vsanQueryService) {
        this.vsanService = vsanService;
        this.vsanQueryService = vsanQueryService;
    }

    /**
     * {@code POST  /vsans} : Create a new vsan.
     *
     * @param vsanDTO the vsanDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new vsanDTO, or with status {@code 400 (Bad Request)} if the vsan has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/vsans")
    public ResponseEntity<VsanDTO> createVsan(@RequestBody VsanDTO vsanDTO) throws URISyntaxException {
        log.debug("REST request to save Vsan : {}", vsanDTO);
        if (vsanDTO.getId() != null) {
            throw new BadRequestAlertException("A new vsan cannot already have an ID", ENTITY_NAME, "idexists");
        }
        VsanDTO result = vsanService.save(vsanDTO);
        return ResponseEntity.created(new URI("/api/vsans/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /vsans} : Updates an existing vsan.
     *
     * @param vsanDTO the vsanDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated vsanDTO,
     * or with status {@code 400 (Bad Request)} if the vsanDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the vsanDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/vsans")
    public ResponseEntity<VsanDTO> updateVsan(@RequestBody VsanDTO vsanDTO) throws URISyntaxException {
        log.debug("REST request to update Vsan : {}", vsanDTO);
        if (vsanDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        VsanDTO result = vsanService.save(vsanDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, vsanDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /vsans} : get all the vsans.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of vsans in body.
     */
    @GetMapping("/vsans")
    public ResponseEntity<List<VsanDTO>> getAllVsans(VsanCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Vsans by criteria: {}", criteria);
        Page<VsanDTO> page = vsanQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /vsans/count} : count all the vsans.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/vsans/count")
    public ResponseEntity<Long> countVsans(VsanCriteria criteria) {
        log.debug("REST request to count Vsans by criteria: {}", criteria);
        return ResponseEntity.ok().body(vsanQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /vsans/:id} : get the "id" vsan.
     *
     * @param id the id of the vsanDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the vsanDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/vsans/{id}")
    public ResponseEntity<VsanDTO> getVsan(@PathVariable Long id) {
        log.debug("REST request to get Vsan : {}", id);
        Optional<VsanDTO> vsanDTO = vsanService.findOne(id);
        return ResponseUtil.wrapOrNotFound(vsanDTO);
    }

    /**
     * {@code DELETE  /vsans/:id} : delete the "id" vsan.
     *
     * @param id the id of the vsanDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/vsans/{id}")
    public ResponseEntity<Void> deleteVsan(@PathVariable Long id) {
        log.debug("REST request to delete Vsan : {}", id);
        vsanService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
