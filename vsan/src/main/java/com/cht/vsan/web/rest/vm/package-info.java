/**
 * View Models used by Spring MVC REST controllers.
 */
package com.cht.vsan.web.rest.vm;
