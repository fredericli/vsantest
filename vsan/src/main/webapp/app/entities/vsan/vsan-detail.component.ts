import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IVsan } from 'app/shared/model/vsan.model';

@Component({
  selector: 'jhi-vsan-detail',
  templateUrl: './vsan-detail.component.html',
})
export class VsanDetailComponent implements OnInit {
  vsan: IVsan | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ vsan }) => (this.vsan = vsan));
  }

  previousState(): void {
    window.history.back();
  }
}
