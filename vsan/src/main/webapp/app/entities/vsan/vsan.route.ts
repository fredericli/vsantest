import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IVsan, Vsan } from 'app/shared/model/vsan.model';
import { VsanService } from './vsan.service';
import { VsanComponent } from './vsan.component';
import { VsanDetailComponent } from './vsan-detail.component';
import { VsanUpdateComponent } from './vsan-update.component';

@Injectable({ providedIn: 'root' })
export class VsanResolve implements Resolve<IVsan> {
  constructor(private service: VsanService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IVsan> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((vsan: HttpResponse<Vsan>) => {
          if (vsan.body) {
            return of(vsan.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Vsan());
  }
}

export const vsanRoute: Routes = [
  {
    path: '',
    component: VsanComponent,
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,asc',
      pageTitle: 'vsanApp.vsan.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: VsanDetailComponent,
    resolve: {
      vsan: VsanResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'vsanApp.vsan.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: VsanUpdateComponent,
    resolve: {
      vsan: VsanResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'vsanApp.vsan.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: VsanUpdateComponent,
    resolve: {
      vsan: VsanResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'vsanApp.vsan.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
