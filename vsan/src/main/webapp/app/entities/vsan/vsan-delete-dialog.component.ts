import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IVsan } from 'app/shared/model/vsan.model';
import { VsanService } from './vsan.service';

@Component({
  templateUrl: './vsan-delete-dialog.component.html',
})
export class VsanDeleteDialogComponent {
  vsan?: IVsan;

  constructor(protected vsanService: VsanService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.vsanService.delete(id).subscribe(() => {
      this.eventManager.broadcast('vsanListModification');
      this.activeModal.close();
    });
  }
}
