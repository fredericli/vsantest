import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IVsan, Vsan } from 'app/shared/model/vsan.model';
import { VsanService } from './vsan.service';

@Component({
  selector: 'jhi-vsan-update',
  templateUrl: './vsan-update.component.html',
})
export class VsanUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    filepath: [],
    dir: [],
    filesize: [],
    status: [],
    fileHash: [],
    fileUuid: [],
    vesUuid: [],
    vv: [],
    vesrr: [],
    rr: [],
    createDate: [],
  });

  constructor(protected vsanService: VsanService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ vsan }) => {
      if (!vsan.id) {
        const today = moment().startOf('day');
        vsan.createDate = today;
      }

      this.updateForm(vsan);
    });
  }

  updateForm(vsan: IVsan): void {
    this.editForm.patchValue({
      id: vsan.id,
      filepath: vsan.filepath,
      dir: vsan.dir,
      filesize: vsan.filesize,
      status: vsan.status,
      fileHash: vsan.fileHash,
      fileUuid: vsan.fileUuid,
      vesUuid: vsan.vesUuid,
      vv: vsan.vv,
      vesrr: vsan.vesrr,
      rr: vsan.rr,
      createDate: vsan.createDate ? vsan.createDate.format(DATE_TIME_FORMAT) : null,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const vsan = this.createFromForm();
    if (vsan.id !== undefined) {
      this.subscribeToSaveResponse(this.vsanService.update(vsan));
    } else {
      this.subscribeToSaveResponse(this.vsanService.create(vsan));
    }
  }

  private createFromForm(): IVsan {
    return {
      ...new Vsan(),
      id: this.editForm.get(['id'])!.value,
      filepath: this.editForm.get(['filepath'])!.value,
      dir: this.editForm.get(['dir'])!.value,
      filesize: this.editForm.get(['filesize'])!.value,
      status: this.editForm.get(['status'])!.value,
      fileHash: this.editForm.get(['fileHash'])!.value,
      fileUuid: this.editForm.get(['fileUuid'])!.value,
      vesUuid: this.editForm.get(['vesUuid'])!.value,
      vv: this.editForm.get(['vv'])!.value,
      vesrr: this.editForm.get(['vesrr'])!.value,
      rr: this.editForm.get(['rr'])!.value,
      createDate: this.editForm.get(['createDate'])!.value ? moment(this.editForm.get(['createDate'])!.value, DATE_TIME_FORMAT) : undefined,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IVsan>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
