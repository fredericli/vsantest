import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { VsanSharedModule } from 'app/shared/shared.module';
import { VsanComponent } from './vsan.component';
import { VsanDetailComponent } from './vsan-detail.component';
import { VsanUpdateComponent } from './vsan-update.component';
import { VsanDeleteDialogComponent } from './vsan-delete-dialog.component';
import { vsanRoute } from './vsan.route';

@NgModule({
  imports: [VsanSharedModule, RouterModule.forChild(vsanRoute)],
  declarations: [VsanComponent, VsanDetailComponent, VsanUpdateComponent, VsanDeleteDialogComponent],
  entryComponents: [VsanDeleteDialogComponent],
})
export class VsanVsanModule {}
