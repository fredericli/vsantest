import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IVsan } from 'app/shared/model/vsan.model';

type EntityResponseType = HttpResponse<IVsan>;
type EntityArrayResponseType = HttpResponse<IVsan[]>;

@Injectable({ providedIn: 'root' })
export class VsanService {
  public resourceUrl = SERVER_API_URL + 'api/vsans';

  constructor(protected http: HttpClient) {}

  create(vsan: IVsan): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(vsan);
    return this.http
      .post<IVsan>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(vsan: IVsan): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(vsan);
    return this.http
      .put<IVsan>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IVsan>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IVsan[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(vsan: IVsan): IVsan {
    const copy: IVsan = Object.assign({}, vsan, {
      createDate: vsan.createDate && vsan.createDate.isValid() ? vsan.createDate.toJSON() : undefined,
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.createDate = res.body.createDate ? moment(res.body.createDate) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((vsan: IVsan) => {
        vsan.createDate = vsan.createDate ? moment(vsan.createDate) : undefined;
      });
    }
    return res;
  }
}
