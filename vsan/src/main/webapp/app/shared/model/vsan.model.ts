import { Moment } from 'moment';

export interface IVsan {
  id?: number;
  filepath?: string;
  dir?: number;
  filesize?: number;
  status?: string;
  fileHash?: string;
  fileUuid?: string;
  vesUuid?: string;
  vv?: string;
  vesrr?: number;
  rr?: number;
  createDate?: Moment;
}

export class Vsan implements IVsan {
  constructor(
    public id?: number,
    public filepath?: string,
    public dir?: number,
    public filesize?: number,
    public status?: string,
    public fileHash?: string,
    public fileUuid?: string,
    public vesUuid?: string,
    public vv?: string,
    public vesrr?: number,
    public rr?: number,
    public createDate?: Moment
  ) {}
}
