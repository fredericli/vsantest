package com.cht.vsan.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.cht.vsan.web.rest.TestUtil;

public class VsanDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(VsanDTO.class);
        VsanDTO vsanDTO1 = new VsanDTO();
        vsanDTO1.setId(1L);
        VsanDTO vsanDTO2 = new VsanDTO();
        assertThat(vsanDTO1).isNotEqualTo(vsanDTO2);
        vsanDTO2.setId(vsanDTO1.getId());
        assertThat(vsanDTO1).isEqualTo(vsanDTO2);
        vsanDTO2.setId(2L);
        assertThat(vsanDTO1).isNotEqualTo(vsanDTO2);
        vsanDTO1.setId(null);
        assertThat(vsanDTO1).isNotEqualTo(vsanDTO2);
    }
}
