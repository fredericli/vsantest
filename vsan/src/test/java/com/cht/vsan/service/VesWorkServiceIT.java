package com.cht.vsan.service;

import com.cht.vsan.utils.FileUtils;
import com.cht.vsan.utils.JsonUtils;
import com.cht.vsan.utils.VesBcRestUtils;
import com.cht.vsan.ves.service.*;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;

@Slf4j
public class VesWorkServiceIT {

    @Test
    public void createBuckets() throws Exception {

        Bucket bucket = new Bucket();
        bucket.setToken("Q0hUSW5zdXJhbmNlSG9zcGl0YWw=");
        bucket.setBucketId(String.valueOf(System.currentTimeMillis()));
        bucket.setBucketName("202106121746");

        String url = "http://210.61.11.128:8088/api/Bucket";

//        String response = new RestTemplate().putForEntity(url, bucket, String.class).getBody();
        BucketRes response = this.putForEntity(BucketRes.class, url, bucket);

        log.info("建立Bucket任務:{}", response);

    }

    @Test
    public void putFile() throws Exception {

        InsFile insFile = new InsFile();
        insFile.setBucketId("202106121746");
        insFile.setComment("VES PT");
        insFile.setInsnoKey("20210612174615-eyxXntlmq");
//        insFile.setPdfdata("JVBERi0xLjQKJeLjz9MKMiAwIG9iago8PC9MZW5ndGggMTAwL0ZpbHRlci9GbGF0ZURlY29kZT4-c3RyZWFtCnicK-RyCuEyNlOwMDBTCEnhMlAwAFH6boYKhkCBNLCAgUJROpeGR2pOTr5CeH5RToqClUJyRklJanFJvJGBkaGBsbGhoYGpsaGprkG8oV5BSppmSBZQZzrMONcQrkAuAErkGakKZW5kc3RyZWFtCmVuZG9iago0IDAgb2JqCjw8L1R5cGUvUGFnZS9NZWRpYUJveFswIDAgNTk1IDg0Ml0vUmVzb3VyY2VzPDwvRm9udDw8L0YxIDEgMCBSPj4-Pi9Db250ZW50cyAyIDAgUi9QYXJlbnQgMyAwIFI-PgplbmRvYmoKMSAwIG9iago8PC9UeXBlL0ZvbnQvU3VidHlwZS9UeXBlMS9CYXNlRm9udC9Db3VyaWVyL0VuY29kaW5nL1dpbkFuc2lFbmNvZGluZz4-CmVuZG9iagozIDAgb2JqCjw8L1R5cGUvUGFnZXMvQ291bnQgMS9LaWRzWzQgMCBSXT4-CmVuZG9iago1IDAgb2JqCjw8L1R5cGUvQ2F0YWxvZy9QYWdlcyAzIDAgUj4-CmVuZG9iago2IDAgb2JqCjw8L1Byb2R1Y2VyKGlUZXh0riA1LjUuMTMuMiCpMjAwMC0yMDIwIGlUZXh0IEdyb3VwIE5WIFwoQUdQTC12ZXJzaW9uXCkpL0NyZWF0aW9uRGF0ZShEOjIwMjEwMzMxMTA1MzE1KzA4JzAwJykvTW9kRGF0ZShEOjIwMjEwMzMxMTA1MzE1KzA4JzAwJyk-PgplbmRvYmoKeHJlZgowIDcKMDAwMDAwMDAwMCA2NTUzNSBmIAowMDAwMDAwMjk0IDAwMDAwIG4gCjAwMDAwMDAwMTUgMDAwMDAgbiAKMDAwMDAwMDM4MCAwMDAwMCBuIAowMDAwMDAwMTgyIDAwMDAwIG4gCjAwMDAwMDA0MzEgMDAwMDAgbiAKMDAwMDAwMDQ3NiAwMDAwMCBuIAp0cmFpbGVyCjw8L1NpemUgNy9Sb290IDUgMCBSL0luZm8gNiAwIFIvSUQgWzxmODdjNGFlYmFkNGYxZWQxNmE4MmUwZDg5NjQzNjU3OT48Zjg3YzRhZWJhZDRmMWVkMTZhODJlMGQ4OTY0MzY1Nzk-XT4-CiVpVGV4dC01LjUuMTMuMgpzdGFydHhyZWYKNjM2CiUlRU9GCg");
        insFile.setPdfdata(this.fileToBase64("/DATA/VSan/202106121741/20210612174151-0EvG17QRwY.big"));
        insFile.setToken("Q0hUSW5zdXJhbmNlSG9zcGl0YWw=");


        String url = "http://210.61.11.128:8088/api/insurance";

//        InsFileRes response = new RestTemplate().postForEntity(url, insFile, InsFileRes.class).getBody();

//        HttpHeaders headers = new HttpHeaders();
//        headers.add("Content-Type" , "application/json");


//        ResponseEntity<InsFileRes> response =
//            new RestTemplate().exchange(url, HttpMethod.PUT, new HttpEntity<>(insFile, headers), InsFileRes.class);

        InsFileRes insFileRes = this.putForEntity(InsFileRes.class, url, insFile);

        log.info("建立檔案任務:{}", insFileRes);

    }

    public <T, R> T putForEntity(Class<T> clazz, String url, R body, Object... uriVariables) throws Exception {
        HttpEntity<R> request = new HttpEntity<>(body);
        ResponseEntity<String> response = new RestTemplate().exchange(url, HttpMethod.PUT, request,
            String.class, uriVariables);
        JavaType javaType = new ObjectMapper().getTypeFactory().constructType(clazz);
        return readValue(response, javaType);
    }

    public <T> T getForEntity(Class<T> clazz, String url, Object... uriVariables) throws Exception {

        ResponseEntity<String> response = new RestTemplate().getForEntity(url, String.class,
            uriVariables);
        JavaType javaType = new ObjectMapper().getTypeFactory().constructType(clazz);
        return readValue(response, javaType);
    }

    private <T> T readValue(ResponseEntity<String> response, JavaType javaType) throws Exception {

        T result = null;
        if (response.getStatusCode() == HttpStatus.OK
            || response.getStatusCode() == HttpStatus.CREATED) {
            try {

                result = new ObjectMapper().readValue(response.getBody(), javaType);
            } catch (Exception e) {

                log.error(ExceptionUtils.getStackTrace(e));
                throw e;
            }
        } else
            log.error("No data found {}", response.getStatusCode());

        return result;
    }

    private String fileToBase64(final String path) throws IOException {
        final byte[] input_file = Files.readAllBytes(Paths.get(path));
        final byte[] encodedBytes = Base64.getEncoder().encode(input_file);
        return new String(encodedBytes);
    }

    //    @SneakyThrows
    @Test
    public void vesToFileContent() throws Exception {

        String url = "http://210.61.11.128:8088/api/insurance";

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url)
            .queryParam("token", "Q0hUSW5zdXJhbmNlSG9zcGl0YWw=")
            .queryParam("bucketId", "202106121829")
            .queryParam("uuidNumber", "20210612182920_4298ef5e-54e2-4c62-9a90-6ef0787687cc");

        log.info("送出去的資料是:{}", builder.toUriString());


        VesFileRes response = this.getForEntity(VesFileRes.class, builder.toUriString());

//        HttpHeaders headers = new HttpHeaders();
//        headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
//
//        HttpEntity<?> entity = new HttpEntity<>(headers);
//
//        HttpEntity<String> response = new RestTemplate().exchange(
//            builder.toUriString(),
//            HttpMethod.GET,
//            entity,
//            String.class);

//        GetVesFileReq req = new GetVesFileReq();
//        req.setToken("Q0hUSW5zdXJhbmNlSG9zcGl0YWw=");
//        req.setBucketId("202106121829");
//        req.setUuidNumber("20210612182920_4298ef5e-54e2-4c62-9a90-6ef0787687cc");
//
//
//
//        GetVesFileRes getVesFileRes = this.getForEntity(GetVesFileRes.class, url, req);

        log.info("取到的值是:{}", response);


    }

    @Test
    public void getVesFile() throws IOException {


        String vesUuid = "20210612182920_4298ef5e-54e2-4c62-9a90-6ef0787687cc";
        VesFileReq vesFileObject = VesFileReq.builder()
            .token("Q0hUSW5zdXJhbmNlSG9zcGl0YWw=")
            .bucketId("202106121829")
            .uuidNumber(vesUuid)
            .build();

        String result = VesBcRestUtils.get(
            "http://210.61.11.128:8088/api/insurance"
            , vesFileObject
        );

        log.info("Ves 取回 檔案 結果：{}", result.length() > 128 ? result.substring(0, 128) + "…" : result);

        VesFileRes vesFileRes = JsonUtils.toObject(VesFileRes.class, result);


        // 把取到的值，返回來
        String fileStr = vesFileRes.getPdfdata();

        String dir = "202106122338";
        String filePath = "/DATA/VVSan/" + dir;
        File file = new File(filePath);
        if (!file.exists()) {
            file.mkdirs();
        }
        filePath = filePath  + "/" + vesUuid;

        FileUtils.base64ToFile(fileStr, filePath);



    }
}
