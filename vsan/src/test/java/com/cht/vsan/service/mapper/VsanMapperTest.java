package com.cht.vsan.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class VsanMapperTest {

    private VsanMapper vsanMapper;

    @BeforeEach
    public void setUp() {
        vsanMapper = new VsanMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(vsanMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(vsanMapper.fromId(null)).isNull();
    }
}
