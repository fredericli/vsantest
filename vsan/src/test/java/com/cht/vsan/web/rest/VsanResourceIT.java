package com.cht.vsan.web.rest;

import com.cht.vsan.VsanApp;
import com.cht.vsan.domain.Vsan;
import com.cht.vsan.repository.VsanRepository;
import com.cht.vsan.service.VsanService;
import com.cht.vsan.service.dto.VsanDTO;
import com.cht.vsan.service.mapper.VsanMapper;
import com.cht.vsan.service.dto.VsanCriteria;
import com.cht.vsan.service.VsanQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link VsanResource} REST controller.
 */
@SpringBootTest(classes = VsanApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class VsanResourceIT {

    private static final String DEFAULT_FILEPATH = "AAAAAAAAAA";
    private static final String UPDATED_FILEPATH = "BBBBBBBBBB";

    private static final Long DEFAULT_DIR = 1L;
    private static final Long UPDATED_DIR = 2L;
    private static final Long SMALLER_DIR = 1L - 1L;

    private static final Double DEFAULT_FILESIZE = 1D;
    private static final Double UPDATED_FILESIZE = 2D;
    private static final Double SMALLER_FILESIZE = 1D - 1D;

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final String DEFAULT_FILE_HASH = "AAAAAAAAAA";
    private static final String UPDATED_FILE_HASH = "BBBBBBBBBB";

    private static final String DEFAULT_FILE_UUID = "AAAAAAAAAA";
    private static final String UPDATED_FILE_UUID = "BBBBBBBBBB";

    private static final String DEFAULT_VES_UUID = "AAAAAAAAAA";
    private static final String UPDATED_VES_UUID = "BBBBBBBBBB";

    private static final String DEFAULT_VV = "AAAAAAAAAA";
    private static final String UPDATED_VV = "BBBBBBBBBB";

    private static final Integer DEFAULT_VESRR = 1;
    private static final Integer UPDATED_VESRR = 2;
    private static final Integer SMALLER_VESRR = 1 - 1;

    private static final Integer DEFAULT_RR = 1;
    private static final Integer UPDATED_RR = 2;
    private static final Integer SMALLER_RR = 1 - 1;

    private static final Instant DEFAULT_CREATE_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATE_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private VsanRepository vsanRepository;

    @Autowired
    private VsanMapper vsanMapper;

    @Autowired
    private VsanService vsanService;

    @Autowired
    private VsanQueryService vsanQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restVsanMockMvc;

    private Vsan vsan;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Vsan createEntity(EntityManager em) {
        Vsan vsan = new Vsan()
            .filepath(DEFAULT_FILEPATH)
            .dir(DEFAULT_DIR)
            .filesize(DEFAULT_FILESIZE)
            .status(DEFAULT_STATUS)
            .fileHash(DEFAULT_FILE_HASH)
            .fileUuid(DEFAULT_FILE_UUID)
            .vesUuid(DEFAULT_VES_UUID)
            .vv(DEFAULT_VV)
            .vesrr(DEFAULT_VESRR)
            .rr(DEFAULT_RR)
            .createDate(DEFAULT_CREATE_DATE);
        return vsan;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Vsan createUpdatedEntity(EntityManager em) {
        Vsan vsan = new Vsan()
            .filepath(UPDATED_FILEPATH)
            .dir(UPDATED_DIR)
            .filesize(UPDATED_FILESIZE)
            .status(UPDATED_STATUS)
            .fileHash(UPDATED_FILE_HASH)
            .fileUuid(UPDATED_FILE_UUID)
            .vesUuid(UPDATED_VES_UUID)
            .vv(UPDATED_VV)
            .vesrr(UPDATED_VESRR)
            .rr(UPDATED_RR)
            .createDate(UPDATED_CREATE_DATE);
        return vsan;
    }

    @BeforeEach
    public void initTest() {
        vsan = createEntity(em);
    }

    @Test
    @Transactional
    public void createVsan() throws Exception {
        int databaseSizeBeforeCreate = vsanRepository.findAll().size();
        // Create the Vsan
        VsanDTO vsanDTO = vsanMapper.toDto(vsan);
        restVsanMockMvc.perform(post("/api/vsans")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(vsanDTO)))
            .andExpect(status().isCreated());

        // Validate the Vsan in the database
        List<Vsan> vsanList = vsanRepository.findAll();
        assertThat(vsanList).hasSize(databaseSizeBeforeCreate + 1);
        Vsan testVsan = vsanList.get(vsanList.size() - 1);
        assertThat(testVsan.getFilepath()).isEqualTo(DEFAULT_FILEPATH);
        assertThat(testVsan.getDir()).isEqualTo(DEFAULT_DIR);
        assertThat(testVsan.getFilesize()).isEqualTo(DEFAULT_FILESIZE);
        assertThat(testVsan.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testVsan.getFileHash()).isEqualTo(DEFAULT_FILE_HASH);
        assertThat(testVsan.getFileUuid()).isEqualTo(DEFAULT_FILE_UUID);
        assertThat(testVsan.getVesUuid()).isEqualTo(DEFAULT_VES_UUID);
        assertThat(testVsan.getVv()).isEqualTo(DEFAULT_VV);
        assertThat(testVsan.getVesrr()).isEqualTo(DEFAULT_VESRR);
        assertThat(testVsan.getRr()).isEqualTo(DEFAULT_RR);
        assertThat(testVsan.getCreateDate()).isEqualTo(DEFAULT_CREATE_DATE);
    }

    @Test
    @Transactional
    public void createVsanWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = vsanRepository.findAll().size();

        // Create the Vsan with an existing ID
        vsan.setId(1L);
        VsanDTO vsanDTO = vsanMapper.toDto(vsan);

        // An entity with an existing ID cannot be created, so this API call must fail
        restVsanMockMvc.perform(post("/api/vsans")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(vsanDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Vsan in the database
        List<Vsan> vsanList = vsanRepository.findAll();
        assertThat(vsanList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllVsans() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList
        restVsanMockMvc.perform(get("/api/vsans?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(vsan.getId().intValue())))
            .andExpect(jsonPath("$.[*].filepath").value(hasItem(DEFAULT_FILEPATH)))
            .andExpect(jsonPath("$.[*].dir").value(hasItem(DEFAULT_DIR.intValue())))
            .andExpect(jsonPath("$.[*].filesize").value(hasItem(DEFAULT_FILESIZE.doubleValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].fileHash").value(hasItem(DEFAULT_FILE_HASH)))
            .andExpect(jsonPath("$.[*].fileUuid").value(hasItem(DEFAULT_FILE_UUID)))
            .andExpect(jsonPath("$.[*].vesUuid").value(hasItem(DEFAULT_VES_UUID)))
            .andExpect(jsonPath("$.[*].vv").value(hasItem(DEFAULT_VV)))
            .andExpect(jsonPath("$.[*].vesrr").value(hasItem(DEFAULT_VESRR)))
            .andExpect(jsonPath("$.[*].rr").value(hasItem(DEFAULT_RR)))
            .andExpect(jsonPath("$.[*].createDate").value(hasItem(DEFAULT_CREATE_DATE.toString())));
    }
    
    @Test
    @Transactional
    public void getVsan() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get the vsan
        restVsanMockMvc.perform(get("/api/vsans/{id}", vsan.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(vsan.getId().intValue()))
            .andExpect(jsonPath("$.filepath").value(DEFAULT_FILEPATH))
            .andExpect(jsonPath("$.dir").value(DEFAULT_DIR.intValue()))
            .andExpect(jsonPath("$.filesize").value(DEFAULT_FILESIZE.doubleValue()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS))
            .andExpect(jsonPath("$.fileHash").value(DEFAULT_FILE_HASH))
            .andExpect(jsonPath("$.fileUuid").value(DEFAULT_FILE_UUID))
            .andExpect(jsonPath("$.vesUuid").value(DEFAULT_VES_UUID))
            .andExpect(jsonPath("$.vv").value(DEFAULT_VV))
            .andExpect(jsonPath("$.vesrr").value(DEFAULT_VESRR))
            .andExpect(jsonPath("$.rr").value(DEFAULT_RR))
            .andExpect(jsonPath("$.createDate").value(DEFAULT_CREATE_DATE.toString()));
    }


    @Test
    @Transactional
    public void getVsansByIdFiltering() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        Long id = vsan.getId();

        defaultVsanShouldBeFound("id.equals=" + id);
        defaultVsanShouldNotBeFound("id.notEquals=" + id);

        defaultVsanShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultVsanShouldNotBeFound("id.greaterThan=" + id);

        defaultVsanShouldBeFound("id.lessThanOrEqual=" + id);
        defaultVsanShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllVsansByFilepathIsEqualToSomething() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where filepath equals to DEFAULT_FILEPATH
        defaultVsanShouldBeFound("filepath.equals=" + DEFAULT_FILEPATH);

        // Get all the vsanList where filepath equals to UPDATED_FILEPATH
        defaultVsanShouldNotBeFound("filepath.equals=" + UPDATED_FILEPATH);
    }

    @Test
    @Transactional
    public void getAllVsansByFilepathIsNotEqualToSomething() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where filepath not equals to DEFAULT_FILEPATH
        defaultVsanShouldNotBeFound("filepath.notEquals=" + DEFAULT_FILEPATH);

        // Get all the vsanList where filepath not equals to UPDATED_FILEPATH
        defaultVsanShouldBeFound("filepath.notEquals=" + UPDATED_FILEPATH);
    }

    @Test
    @Transactional
    public void getAllVsansByFilepathIsInShouldWork() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where filepath in DEFAULT_FILEPATH or UPDATED_FILEPATH
        defaultVsanShouldBeFound("filepath.in=" + DEFAULT_FILEPATH + "," + UPDATED_FILEPATH);

        // Get all the vsanList where filepath equals to UPDATED_FILEPATH
        defaultVsanShouldNotBeFound("filepath.in=" + UPDATED_FILEPATH);
    }

    @Test
    @Transactional
    public void getAllVsansByFilepathIsNullOrNotNull() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where filepath is not null
        defaultVsanShouldBeFound("filepath.specified=true");

        // Get all the vsanList where filepath is null
        defaultVsanShouldNotBeFound("filepath.specified=false");
    }
                @Test
    @Transactional
    public void getAllVsansByFilepathContainsSomething() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where filepath contains DEFAULT_FILEPATH
        defaultVsanShouldBeFound("filepath.contains=" + DEFAULT_FILEPATH);

        // Get all the vsanList where filepath contains UPDATED_FILEPATH
        defaultVsanShouldNotBeFound("filepath.contains=" + UPDATED_FILEPATH);
    }

    @Test
    @Transactional
    public void getAllVsansByFilepathNotContainsSomething() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where filepath does not contain DEFAULT_FILEPATH
        defaultVsanShouldNotBeFound("filepath.doesNotContain=" + DEFAULT_FILEPATH);

        // Get all the vsanList where filepath does not contain UPDATED_FILEPATH
        defaultVsanShouldBeFound("filepath.doesNotContain=" + UPDATED_FILEPATH);
    }


    @Test
    @Transactional
    public void getAllVsansByDirIsEqualToSomething() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where dir equals to DEFAULT_DIR
        defaultVsanShouldBeFound("dir.equals=" + DEFAULT_DIR);

        // Get all the vsanList where dir equals to UPDATED_DIR
        defaultVsanShouldNotBeFound("dir.equals=" + UPDATED_DIR);
    }

    @Test
    @Transactional
    public void getAllVsansByDirIsNotEqualToSomething() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where dir not equals to DEFAULT_DIR
        defaultVsanShouldNotBeFound("dir.notEquals=" + DEFAULT_DIR);

        // Get all the vsanList where dir not equals to UPDATED_DIR
        defaultVsanShouldBeFound("dir.notEquals=" + UPDATED_DIR);
    }

    @Test
    @Transactional
    public void getAllVsansByDirIsInShouldWork() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where dir in DEFAULT_DIR or UPDATED_DIR
        defaultVsanShouldBeFound("dir.in=" + DEFAULT_DIR + "," + UPDATED_DIR);

        // Get all the vsanList where dir equals to UPDATED_DIR
        defaultVsanShouldNotBeFound("dir.in=" + UPDATED_DIR);
    }

    @Test
    @Transactional
    public void getAllVsansByDirIsNullOrNotNull() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where dir is not null
        defaultVsanShouldBeFound("dir.specified=true");

        // Get all the vsanList where dir is null
        defaultVsanShouldNotBeFound("dir.specified=false");
    }

    @Test
    @Transactional
    public void getAllVsansByDirIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where dir is greater than or equal to DEFAULT_DIR
        defaultVsanShouldBeFound("dir.greaterThanOrEqual=" + DEFAULT_DIR);

        // Get all the vsanList where dir is greater than or equal to UPDATED_DIR
        defaultVsanShouldNotBeFound("dir.greaterThanOrEqual=" + UPDATED_DIR);
    }

    @Test
    @Transactional
    public void getAllVsansByDirIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where dir is less than or equal to DEFAULT_DIR
        defaultVsanShouldBeFound("dir.lessThanOrEqual=" + DEFAULT_DIR);

        // Get all the vsanList where dir is less than or equal to SMALLER_DIR
        defaultVsanShouldNotBeFound("dir.lessThanOrEqual=" + SMALLER_DIR);
    }

    @Test
    @Transactional
    public void getAllVsansByDirIsLessThanSomething() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where dir is less than DEFAULT_DIR
        defaultVsanShouldNotBeFound("dir.lessThan=" + DEFAULT_DIR);

        // Get all the vsanList where dir is less than UPDATED_DIR
        defaultVsanShouldBeFound("dir.lessThan=" + UPDATED_DIR);
    }

    @Test
    @Transactional
    public void getAllVsansByDirIsGreaterThanSomething() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where dir is greater than DEFAULT_DIR
        defaultVsanShouldNotBeFound("dir.greaterThan=" + DEFAULT_DIR);

        // Get all the vsanList where dir is greater than SMALLER_DIR
        defaultVsanShouldBeFound("dir.greaterThan=" + SMALLER_DIR);
    }


    @Test
    @Transactional
    public void getAllVsansByFilesizeIsEqualToSomething() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where filesize equals to DEFAULT_FILESIZE
        defaultVsanShouldBeFound("filesize.equals=" + DEFAULT_FILESIZE);

        // Get all the vsanList where filesize equals to UPDATED_FILESIZE
        defaultVsanShouldNotBeFound("filesize.equals=" + UPDATED_FILESIZE);
    }

    @Test
    @Transactional
    public void getAllVsansByFilesizeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where filesize not equals to DEFAULT_FILESIZE
        defaultVsanShouldNotBeFound("filesize.notEquals=" + DEFAULT_FILESIZE);

        // Get all the vsanList where filesize not equals to UPDATED_FILESIZE
        defaultVsanShouldBeFound("filesize.notEquals=" + UPDATED_FILESIZE);
    }

    @Test
    @Transactional
    public void getAllVsansByFilesizeIsInShouldWork() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where filesize in DEFAULT_FILESIZE or UPDATED_FILESIZE
        defaultVsanShouldBeFound("filesize.in=" + DEFAULT_FILESIZE + "," + UPDATED_FILESIZE);

        // Get all the vsanList where filesize equals to UPDATED_FILESIZE
        defaultVsanShouldNotBeFound("filesize.in=" + UPDATED_FILESIZE);
    }

    @Test
    @Transactional
    public void getAllVsansByFilesizeIsNullOrNotNull() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where filesize is not null
        defaultVsanShouldBeFound("filesize.specified=true");

        // Get all the vsanList where filesize is null
        defaultVsanShouldNotBeFound("filesize.specified=false");
    }

    @Test
    @Transactional
    public void getAllVsansByFilesizeIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where filesize is greater than or equal to DEFAULT_FILESIZE
        defaultVsanShouldBeFound("filesize.greaterThanOrEqual=" + DEFAULT_FILESIZE);

        // Get all the vsanList where filesize is greater than or equal to UPDATED_FILESIZE
        defaultVsanShouldNotBeFound("filesize.greaterThanOrEqual=" + UPDATED_FILESIZE);
    }

    @Test
    @Transactional
    public void getAllVsansByFilesizeIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where filesize is less than or equal to DEFAULT_FILESIZE
        defaultVsanShouldBeFound("filesize.lessThanOrEqual=" + DEFAULT_FILESIZE);

        // Get all the vsanList where filesize is less than or equal to SMALLER_FILESIZE
        defaultVsanShouldNotBeFound("filesize.lessThanOrEqual=" + SMALLER_FILESIZE);
    }

    @Test
    @Transactional
    public void getAllVsansByFilesizeIsLessThanSomething() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where filesize is less than DEFAULT_FILESIZE
        defaultVsanShouldNotBeFound("filesize.lessThan=" + DEFAULT_FILESIZE);

        // Get all the vsanList where filesize is less than UPDATED_FILESIZE
        defaultVsanShouldBeFound("filesize.lessThan=" + UPDATED_FILESIZE);
    }

    @Test
    @Transactional
    public void getAllVsansByFilesizeIsGreaterThanSomething() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where filesize is greater than DEFAULT_FILESIZE
        defaultVsanShouldNotBeFound("filesize.greaterThan=" + DEFAULT_FILESIZE);

        // Get all the vsanList where filesize is greater than SMALLER_FILESIZE
        defaultVsanShouldBeFound("filesize.greaterThan=" + SMALLER_FILESIZE);
    }


    @Test
    @Transactional
    public void getAllVsansByStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where status equals to DEFAULT_STATUS
        defaultVsanShouldBeFound("status.equals=" + DEFAULT_STATUS);

        // Get all the vsanList where status equals to UPDATED_STATUS
        defaultVsanShouldNotBeFound("status.equals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllVsansByStatusIsNotEqualToSomething() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where status not equals to DEFAULT_STATUS
        defaultVsanShouldNotBeFound("status.notEquals=" + DEFAULT_STATUS);

        // Get all the vsanList where status not equals to UPDATED_STATUS
        defaultVsanShouldBeFound("status.notEquals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllVsansByStatusIsInShouldWork() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where status in DEFAULT_STATUS or UPDATED_STATUS
        defaultVsanShouldBeFound("status.in=" + DEFAULT_STATUS + "," + UPDATED_STATUS);

        // Get all the vsanList where status equals to UPDATED_STATUS
        defaultVsanShouldNotBeFound("status.in=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllVsansByStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where status is not null
        defaultVsanShouldBeFound("status.specified=true");

        // Get all the vsanList where status is null
        defaultVsanShouldNotBeFound("status.specified=false");
    }
                @Test
    @Transactional
    public void getAllVsansByStatusContainsSomething() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where status contains DEFAULT_STATUS
        defaultVsanShouldBeFound("status.contains=" + DEFAULT_STATUS);

        // Get all the vsanList where status contains UPDATED_STATUS
        defaultVsanShouldNotBeFound("status.contains=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllVsansByStatusNotContainsSomething() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where status does not contain DEFAULT_STATUS
        defaultVsanShouldNotBeFound("status.doesNotContain=" + DEFAULT_STATUS);

        // Get all the vsanList where status does not contain UPDATED_STATUS
        defaultVsanShouldBeFound("status.doesNotContain=" + UPDATED_STATUS);
    }


    @Test
    @Transactional
    public void getAllVsansByFileHashIsEqualToSomething() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where fileHash equals to DEFAULT_FILE_HASH
        defaultVsanShouldBeFound("fileHash.equals=" + DEFAULT_FILE_HASH);

        // Get all the vsanList where fileHash equals to UPDATED_FILE_HASH
        defaultVsanShouldNotBeFound("fileHash.equals=" + UPDATED_FILE_HASH);
    }

    @Test
    @Transactional
    public void getAllVsansByFileHashIsNotEqualToSomething() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where fileHash not equals to DEFAULT_FILE_HASH
        defaultVsanShouldNotBeFound("fileHash.notEquals=" + DEFAULT_FILE_HASH);

        // Get all the vsanList where fileHash not equals to UPDATED_FILE_HASH
        defaultVsanShouldBeFound("fileHash.notEquals=" + UPDATED_FILE_HASH);
    }

    @Test
    @Transactional
    public void getAllVsansByFileHashIsInShouldWork() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where fileHash in DEFAULT_FILE_HASH or UPDATED_FILE_HASH
        defaultVsanShouldBeFound("fileHash.in=" + DEFAULT_FILE_HASH + "," + UPDATED_FILE_HASH);

        // Get all the vsanList where fileHash equals to UPDATED_FILE_HASH
        defaultVsanShouldNotBeFound("fileHash.in=" + UPDATED_FILE_HASH);
    }

    @Test
    @Transactional
    public void getAllVsansByFileHashIsNullOrNotNull() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where fileHash is not null
        defaultVsanShouldBeFound("fileHash.specified=true");

        // Get all the vsanList where fileHash is null
        defaultVsanShouldNotBeFound("fileHash.specified=false");
    }
                @Test
    @Transactional
    public void getAllVsansByFileHashContainsSomething() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where fileHash contains DEFAULT_FILE_HASH
        defaultVsanShouldBeFound("fileHash.contains=" + DEFAULT_FILE_HASH);

        // Get all the vsanList where fileHash contains UPDATED_FILE_HASH
        defaultVsanShouldNotBeFound("fileHash.contains=" + UPDATED_FILE_HASH);
    }

    @Test
    @Transactional
    public void getAllVsansByFileHashNotContainsSomething() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where fileHash does not contain DEFAULT_FILE_HASH
        defaultVsanShouldNotBeFound("fileHash.doesNotContain=" + DEFAULT_FILE_HASH);

        // Get all the vsanList where fileHash does not contain UPDATED_FILE_HASH
        defaultVsanShouldBeFound("fileHash.doesNotContain=" + UPDATED_FILE_HASH);
    }


    @Test
    @Transactional
    public void getAllVsansByFileUuidIsEqualToSomething() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where fileUuid equals to DEFAULT_FILE_UUID
        defaultVsanShouldBeFound("fileUuid.equals=" + DEFAULT_FILE_UUID);

        // Get all the vsanList where fileUuid equals to UPDATED_FILE_UUID
        defaultVsanShouldNotBeFound("fileUuid.equals=" + UPDATED_FILE_UUID);
    }

    @Test
    @Transactional
    public void getAllVsansByFileUuidIsNotEqualToSomething() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where fileUuid not equals to DEFAULT_FILE_UUID
        defaultVsanShouldNotBeFound("fileUuid.notEquals=" + DEFAULT_FILE_UUID);

        // Get all the vsanList where fileUuid not equals to UPDATED_FILE_UUID
        defaultVsanShouldBeFound("fileUuid.notEquals=" + UPDATED_FILE_UUID);
    }

    @Test
    @Transactional
    public void getAllVsansByFileUuidIsInShouldWork() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where fileUuid in DEFAULT_FILE_UUID or UPDATED_FILE_UUID
        defaultVsanShouldBeFound("fileUuid.in=" + DEFAULT_FILE_UUID + "," + UPDATED_FILE_UUID);

        // Get all the vsanList where fileUuid equals to UPDATED_FILE_UUID
        defaultVsanShouldNotBeFound("fileUuid.in=" + UPDATED_FILE_UUID);
    }

    @Test
    @Transactional
    public void getAllVsansByFileUuidIsNullOrNotNull() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where fileUuid is not null
        defaultVsanShouldBeFound("fileUuid.specified=true");

        // Get all the vsanList where fileUuid is null
        defaultVsanShouldNotBeFound("fileUuid.specified=false");
    }
                @Test
    @Transactional
    public void getAllVsansByFileUuidContainsSomething() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where fileUuid contains DEFAULT_FILE_UUID
        defaultVsanShouldBeFound("fileUuid.contains=" + DEFAULT_FILE_UUID);

        // Get all the vsanList where fileUuid contains UPDATED_FILE_UUID
        defaultVsanShouldNotBeFound("fileUuid.contains=" + UPDATED_FILE_UUID);
    }

    @Test
    @Transactional
    public void getAllVsansByFileUuidNotContainsSomething() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where fileUuid does not contain DEFAULT_FILE_UUID
        defaultVsanShouldNotBeFound("fileUuid.doesNotContain=" + DEFAULT_FILE_UUID);

        // Get all the vsanList where fileUuid does not contain UPDATED_FILE_UUID
        defaultVsanShouldBeFound("fileUuid.doesNotContain=" + UPDATED_FILE_UUID);
    }


    @Test
    @Transactional
    public void getAllVsansByVesUuidIsEqualToSomething() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where vesUuid equals to DEFAULT_VES_UUID
        defaultVsanShouldBeFound("vesUuid.equals=" + DEFAULT_VES_UUID);

        // Get all the vsanList where vesUuid equals to UPDATED_VES_UUID
        defaultVsanShouldNotBeFound("vesUuid.equals=" + UPDATED_VES_UUID);
    }

    @Test
    @Transactional
    public void getAllVsansByVesUuidIsNotEqualToSomething() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where vesUuid not equals to DEFAULT_VES_UUID
        defaultVsanShouldNotBeFound("vesUuid.notEquals=" + DEFAULT_VES_UUID);

        // Get all the vsanList where vesUuid not equals to UPDATED_VES_UUID
        defaultVsanShouldBeFound("vesUuid.notEquals=" + UPDATED_VES_UUID);
    }

    @Test
    @Transactional
    public void getAllVsansByVesUuidIsInShouldWork() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where vesUuid in DEFAULT_VES_UUID or UPDATED_VES_UUID
        defaultVsanShouldBeFound("vesUuid.in=" + DEFAULT_VES_UUID + "," + UPDATED_VES_UUID);

        // Get all the vsanList where vesUuid equals to UPDATED_VES_UUID
        defaultVsanShouldNotBeFound("vesUuid.in=" + UPDATED_VES_UUID);
    }

    @Test
    @Transactional
    public void getAllVsansByVesUuidIsNullOrNotNull() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where vesUuid is not null
        defaultVsanShouldBeFound("vesUuid.specified=true");

        // Get all the vsanList where vesUuid is null
        defaultVsanShouldNotBeFound("vesUuid.specified=false");
    }
                @Test
    @Transactional
    public void getAllVsansByVesUuidContainsSomething() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where vesUuid contains DEFAULT_VES_UUID
        defaultVsanShouldBeFound("vesUuid.contains=" + DEFAULT_VES_UUID);

        // Get all the vsanList where vesUuid contains UPDATED_VES_UUID
        defaultVsanShouldNotBeFound("vesUuid.contains=" + UPDATED_VES_UUID);
    }

    @Test
    @Transactional
    public void getAllVsansByVesUuidNotContainsSomething() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where vesUuid does not contain DEFAULT_VES_UUID
        defaultVsanShouldNotBeFound("vesUuid.doesNotContain=" + DEFAULT_VES_UUID);

        // Get all the vsanList where vesUuid does not contain UPDATED_VES_UUID
        defaultVsanShouldBeFound("vesUuid.doesNotContain=" + UPDATED_VES_UUID);
    }


    @Test
    @Transactional
    public void getAllVsansByVvIsEqualToSomething() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where vv equals to DEFAULT_VV
        defaultVsanShouldBeFound("vv.equals=" + DEFAULT_VV);

        // Get all the vsanList where vv equals to UPDATED_VV
        defaultVsanShouldNotBeFound("vv.equals=" + UPDATED_VV);
    }

    @Test
    @Transactional
    public void getAllVsansByVvIsNotEqualToSomething() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where vv not equals to DEFAULT_VV
        defaultVsanShouldNotBeFound("vv.notEquals=" + DEFAULT_VV);

        // Get all the vsanList where vv not equals to UPDATED_VV
        defaultVsanShouldBeFound("vv.notEquals=" + UPDATED_VV);
    }

    @Test
    @Transactional
    public void getAllVsansByVvIsInShouldWork() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where vv in DEFAULT_VV or UPDATED_VV
        defaultVsanShouldBeFound("vv.in=" + DEFAULT_VV + "," + UPDATED_VV);

        // Get all the vsanList where vv equals to UPDATED_VV
        defaultVsanShouldNotBeFound("vv.in=" + UPDATED_VV);
    }

    @Test
    @Transactional
    public void getAllVsansByVvIsNullOrNotNull() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where vv is not null
        defaultVsanShouldBeFound("vv.specified=true");

        // Get all the vsanList where vv is null
        defaultVsanShouldNotBeFound("vv.specified=false");
    }
                @Test
    @Transactional
    public void getAllVsansByVvContainsSomething() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where vv contains DEFAULT_VV
        defaultVsanShouldBeFound("vv.contains=" + DEFAULT_VV);

        // Get all the vsanList where vv contains UPDATED_VV
        defaultVsanShouldNotBeFound("vv.contains=" + UPDATED_VV);
    }

    @Test
    @Transactional
    public void getAllVsansByVvNotContainsSomething() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where vv does not contain DEFAULT_VV
        defaultVsanShouldNotBeFound("vv.doesNotContain=" + DEFAULT_VV);

        // Get all the vsanList where vv does not contain UPDATED_VV
        defaultVsanShouldBeFound("vv.doesNotContain=" + UPDATED_VV);
    }


    @Test
    @Transactional
    public void getAllVsansByVesrrIsEqualToSomething() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where vesrr equals to DEFAULT_VESRR
        defaultVsanShouldBeFound("vesrr.equals=" + DEFAULT_VESRR);

        // Get all the vsanList where vesrr equals to UPDATED_VESRR
        defaultVsanShouldNotBeFound("vesrr.equals=" + UPDATED_VESRR);
    }

    @Test
    @Transactional
    public void getAllVsansByVesrrIsNotEqualToSomething() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where vesrr not equals to DEFAULT_VESRR
        defaultVsanShouldNotBeFound("vesrr.notEquals=" + DEFAULT_VESRR);

        // Get all the vsanList where vesrr not equals to UPDATED_VESRR
        defaultVsanShouldBeFound("vesrr.notEquals=" + UPDATED_VESRR);
    }

    @Test
    @Transactional
    public void getAllVsansByVesrrIsInShouldWork() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where vesrr in DEFAULT_VESRR or UPDATED_VESRR
        defaultVsanShouldBeFound("vesrr.in=" + DEFAULT_VESRR + "," + UPDATED_VESRR);

        // Get all the vsanList where vesrr equals to UPDATED_VESRR
        defaultVsanShouldNotBeFound("vesrr.in=" + UPDATED_VESRR);
    }

    @Test
    @Transactional
    public void getAllVsansByVesrrIsNullOrNotNull() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where vesrr is not null
        defaultVsanShouldBeFound("vesrr.specified=true");

        // Get all the vsanList where vesrr is null
        defaultVsanShouldNotBeFound("vesrr.specified=false");
    }

    @Test
    @Transactional
    public void getAllVsansByVesrrIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where vesrr is greater than or equal to DEFAULT_VESRR
        defaultVsanShouldBeFound("vesrr.greaterThanOrEqual=" + DEFAULT_VESRR);

        // Get all the vsanList where vesrr is greater than or equal to UPDATED_VESRR
        defaultVsanShouldNotBeFound("vesrr.greaterThanOrEqual=" + UPDATED_VESRR);
    }

    @Test
    @Transactional
    public void getAllVsansByVesrrIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where vesrr is less than or equal to DEFAULT_VESRR
        defaultVsanShouldBeFound("vesrr.lessThanOrEqual=" + DEFAULT_VESRR);

        // Get all the vsanList where vesrr is less than or equal to SMALLER_VESRR
        defaultVsanShouldNotBeFound("vesrr.lessThanOrEqual=" + SMALLER_VESRR);
    }

    @Test
    @Transactional
    public void getAllVsansByVesrrIsLessThanSomething() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where vesrr is less than DEFAULT_VESRR
        defaultVsanShouldNotBeFound("vesrr.lessThan=" + DEFAULT_VESRR);

        // Get all the vsanList where vesrr is less than UPDATED_VESRR
        defaultVsanShouldBeFound("vesrr.lessThan=" + UPDATED_VESRR);
    }

    @Test
    @Transactional
    public void getAllVsansByVesrrIsGreaterThanSomething() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where vesrr is greater than DEFAULT_VESRR
        defaultVsanShouldNotBeFound("vesrr.greaterThan=" + DEFAULT_VESRR);

        // Get all the vsanList where vesrr is greater than SMALLER_VESRR
        defaultVsanShouldBeFound("vesrr.greaterThan=" + SMALLER_VESRR);
    }


    @Test
    @Transactional
    public void getAllVsansByRrIsEqualToSomething() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where rr equals to DEFAULT_RR
        defaultVsanShouldBeFound("rr.equals=" + DEFAULT_RR);

        // Get all the vsanList where rr equals to UPDATED_RR
        defaultVsanShouldNotBeFound("rr.equals=" + UPDATED_RR);
    }

    @Test
    @Transactional
    public void getAllVsansByRrIsNotEqualToSomething() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where rr not equals to DEFAULT_RR
        defaultVsanShouldNotBeFound("rr.notEquals=" + DEFAULT_RR);

        // Get all the vsanList where rr not equals to UPDATED_RR
        defaultVsanShouldBeFound("rr.notEquals=" + UPDATED_RR);
    }

    @Test
    @Transactional
    public void getAllVsansByRrIsInShouldWork() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where rr in DEFAULT_RR or UPDATED_RR
        defaultVsanShouldBeFound("rr.in=" + DEFAULT_RR + "," + UPDATED_RR);

        // Get all the vsanList where rr equals to UPDATED_RR
        defaultVsanShouldNotBeFound("rr.in=" + UPDATED_RR);
    }

    @Test
    @Transactional
    public void getAllVsansByRrIsNullOrNotNull() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where rr is not null
        defaultVsanShouldBeFound("rr.specified=true");

        // Get all the vsanList where rr is null
        defaultVsanShouldNotBeFound("rr.specified=false");
    }

    @Test
    @Transactional
    public void getAllVsansByRrIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where rr is greater than or equal to DEFAULT_RR
        defaultVsanShouldBeFound("rr.greaterThanOrEqual=" + DEFAULT_RR);

        // Get all the vsanList where rr is greater than or equal to UPDATED_RR
        defaultVsanShouldNotBeFound("rr.greaterThanOrEqual=" + UPDATED_RR);
    }

    @Test
    @Transactional
    public void getAllVsansByRrIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where rr is less than or equal to DEFAULT_RR
        defaultVsanShouldBeFound("rr.lessThanOrEqual=" + DEFAULT_RR);

        // Get all the vsanList where rr is less than or equal to SMALLER_RR
        defaultVsanShouldNotBeFound("rr.lessThanOrEqual=" + SMALLER_RR);
    }

    @Test
    @Transactional
    public void getAllVsansByRrIsLessThanSomething() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where rr is less than DEFAULT_RR
        defaultVsanShouldNotBeFound("rr.lessThan=" + DEFAULT_RR);

        // Get all the vsanList where rr is less than UPDATED_RR
        defaultVsanShouldBeFound("rr.lessThan=" + UPDATED_RR);
    }

    @Test
    @Transactional
    public void getAllVsansByRrIsGreaterThanSomething() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where rr is greater than DEFAULT_RR
        defaultVsanShouldNotBeFound("rr.greaterThan=" + DEFAULT_RR);

        // Get all the vsanList where rr is greater than SMALLER_RR
        defaultVsanShouldBeFound("rr.greaterThan=" + SMALLER_RR);
    }


    @Test
    @Transactional
    public void getAllVsansByCreateDateIsEqualToSomething() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where createDate equals to DEFAULT_CREATE_DATE
        defaultVsanShouldBeFound("createDate.equals=" + DEFAULT_CREATE_DATE);

        // Get all the vsanList where createDate equals to UPDATED_CREATE_DATE
        defaultVsanShouldNotBeFound("createDate.equals=" + UPDATED_CREATE_DATE);
    }

    @Test
    @Transactional
    public void getAllVsansByCreateDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where createDate not equals to DEFAULT_CREATE_DATE
        defaultVsanShouldNotBeFound("createDate.notEquals=" + DEFAULT_CREATE_DATE);

        // Get all the vsanList where createDate not equals to UPDATED_CREATE_DATE
        defaultVsanShouldBeFound("createDate.notEquals=" + UPDATED_CREATE_DATE);
    }

    @Test
    @Transactional
    public void getAllVsansByCreateDateIsInShouldWork() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where createDate in DEFAULT_CREATE_DATE or UPDATED_CREATE_DATE
        defaultVsanShouldBeFound("createDate.in=" + DEFAULT_CREATE_DATE + "," + UPDATED_CREATE_DATE);

        // Get all the vsanList where createDate equals to UPDATED_CREATE_DATE
        defaultVsanShouldNotBeFound("createDate.in=" + UPDATED_CREATE_DATE);
    }

    @Test
    @Transactional
    public void getAllVsansByCreateDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        // Get all the vsanList where createDate is not null
        defaultVsanShouldBeFound("createDate.specified=true");

        // Get all the vsanList where createDate is null
        defaultVsanShouldNotBeFound("createDate.specified=false");
    }
    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultVsanShouldBeFound(String filter) throws Exception {
        restVsanMockMvc.perform(get("/api/vsans?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(vsan.getId().intValue())))
            .andExpect(jsonPath("$.[*].filepath").value(hasItem(DEFAULT_FILEPATH)))
            .andExpect(jsonPath("$.[*].dir").value(hasItem(DEFAULT_DIR.intValue())))
            .andExpect(jsonPath("$.[*].filesize").value(hasItem(DEFAULT_FILESIZE.doubleValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].fileHash").value(hasItem(DEFAULT_FILE_HASH)))
            .andExpect(jsonPath("$.[*].fileUuid").value(hasItem(DEFAULT_FILE_UUID)))
            .andExpect(jsonPath("$.[*].vesUuid").value(hasItem(DEFAULT_VES_UUID)))
            .andExpect(jsonPath("$.[*].vv").value(hasItem(DEFAULT_VV)))
            .andExpect(jsonPath("$.[*].vesrr").value(hasItem(DEFAULT_VESRR)))
            .andExpect(jsonPath("$.[*].rr").value(hasItem(DEFAULT_RR)))
            .andExpect(jsonPath("$.[*].createDate").value(hasItem(DEFAULT_CREATE_DATE.toString())));

        // Check, that the count call also returns 1
        restVsanMockMvc.perform(get("/api/vsans/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultVsanShouldNotBeFound(String filter) throws Exception {
        restVsanMockMvc.perform(get("/api/vsans?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restVsanMockMvc.perform(get("/api/vsans/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingVsan() throws Exception {
        // Get the vsan
        restVsanMockMvc.perform(get("/api/vsans/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateVsan() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        int databaseSizeBeforeUpdate = vsanRepository.findAll().size();

        // Update the vsan
        Vsan updatedVsan = vsanRepository.findById(vsan.getId()).get();
        // Disconnect from session so that the updates on updatedVsan are not directly saved in db
        em.detach(updatedVsan);
        updatedVsan
            .filepath(UPDATED_FILEPATH)
            .dir(UPDATED_DIR)
            .filesize(UPDATED_FILESIZE)
            .status(UPDATED_STATUS)
            .fileHash(UPDATED_FILE_HASH)
            .fileUuid(UPDATED_FILE_UUID)
            .vesUuid(UPDATED_VES_UUID)
            .vv(UPDATED_VV)
            .vesrr(UPDATED_VESRR)
            .rr(UPDATED_RR)
            .createDate(UPDATED_CREATE_DATE);
        VsanDTO vsanDTO = vsanMapper.toDto(updatedVsan);

        restVsanMockMvc.perform(put("/api/vsans")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(vsanDTO)))
            .andExpect(status().isOk());

        // Validate the Vsan in the database
        List<Vsan> vsanList = vsanRepository.findAll();
        assertThat(vsanList).hasSize(databaseSizeBeforeUpdate);
        Vsan testVsan = vsanList.get(vsanList.size() - 1);
        assertThat(testVsan.getFilepath()).isEqualTo(UPDATED_FILEPATH);
        assertThat(testVsan.getDir()).isEqualTo(UPDATED_DIR);
        assertThat(testVsan.getFilesize()).isEqualTo(UPDATED_FILESIZE);
        assertThat(testVsan.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testVsan.getFileHash()).isEqualTo(UPDATED_FILE_HASH);
        assertThat(testVsan.getFileUuid()).isEqualTo(UPDATED_FILE_UUID);
        assertThat(testVsan.getVesUuid()).isEqualTo(UPDATED_VES_UUID);
        assertThat(testVsan.getVv()).isEqualTo(UPDATED_VV);
        assertThat(testVsan.getVesrr()).isEqualTo(UPDATED_VESRR);
        assertThat(testVsan.getRr()).isEqualTo(UPDATED_RR);
        assertThat(testVsan.getCreateDate()).isEqualTo(UPDATED_CREATE_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingVsan() throws Exception {
        int databaseSizeBeforeUpdate = vsanRepository.findAll().size();

        // Create the Vsan
        VsanDTO vsanDTO = vsanMapper.toDto(vsan);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restVsanMockMvc.perform(put("/api/vsans")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(vsanDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Vsan in the database
        List<Vsan> vsanList = vsanRepository.findAll();
        assertThat(vsanList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteVsan() throws Exception {
        // Initialize the database
        vsanRepository.saveAndFlush(vsan);

        int databaseSizeBeforeDelete = vsanRepository.findAll().size();

        // Delete the vsan
        restVsanMockMvc.perform(delete("/api/vsans/{id}", vsan.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Vsan> vsanList = vsanRepository.findAll();
        assertThat(vsanList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
