package com.cht.vsan.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.cht.vsan.web.rest.TestUtil;

public class VsanTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Vsan.class);
        Vsan vsan1 = new Vsan();
        vsan1.setId(1L);
        Vsan vsan2 = new Vsan();
        vsan2.setId(vsan1.getId());
        assertThat(vsan1).isEqualTo(vsan2);
        vsan2.setId(2L);
        assertThat(vsan1).isNotEqualTo(vsan2);
        vsan1.setId(null);
        assertThat(vsan1).isNotEqualTo(vsan2);
    }
}
