package com.cht.vsan;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.math.BigDecimal;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

class ArchTest {

    @Test
    public void vv() {

        //        BigDecimal b1 = new BigDecimal(new
        // File("/System/Volumes/Data/DATA/VSan/26711873/20201015055300-bm72bMkEpl.big")
        //            .length());
        //        BigDecimal b2 = new BigDecimal(1024);
        //
        //
        //
        //        System.out.println(new
        // File("/System/Volumes/Data/DATA/VSan/26711873/20201015055300-bm72bMkEpl.big")
        //            .length());
        //        double value =
        //                new
        // File("/System/Volumes/Data/DATA/VSan/26711873/20201015055300-bm72bMkEpl.big")
        //                                .length()
        //                        / 1024;
        System.out.println(
                div(
                        new File(
                                        "/System/Volumes/Data/DATA/VSan/26711873/20201015055300-bm72bMkEpl.big")
                                .length(),
                        1024*1024,
                        10));
    }


    public static double div(double v1,double v2,int scale){
        if(scale<0){
            throw new IllegalArgumentException(
                "The scale must be a positive integer or zero");
        }
        BigDecimal b1 = new BigDecimal(Double.toString(v1));
        BigDecimal b2 = new BigDecimal(Double.toString(v2));
        return b1.divide(b2,scale,BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {

        JavaClasses importedClasses =
                new ClassFileImporter()
                        .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
                        .importPackages("com.cht.vsan");

        noClasses()
                .that()
                .resideInAnyPackage("com.cht.vsan.service..")
                .or()
                .resideInAnyPackage("com.cht.vsan.repository..")
                .should()
                .dependOnClassesThat()
                .resideInAnyPackage("..com.cht.vsan.web..")
                .because("Services and repositories should not depend on web layer")
                .check(importedClasses);
    }
}
