import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { VsanTestModule } from '../../../test.module';
import { VsanUpdateComponent } from 'app/entities/vsan/vsan-update.component';
import { VsanService } from 'app/entities/vsan/vsan.service';
import { Vsan } from 'app/shared/model/vsan.model';

describe('Component Tests', () => {
  describe('Vsan Management Update Component', () => {
    let comp: VsanUpdateComponent;
    let fixture: ComponentFixture<VsanUpdateComponent>;
    let service: VsanService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [VsanTestModule],
        declarations: [VsanUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(VsanUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(VsanUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(VsanService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Vsan(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Vsan();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
