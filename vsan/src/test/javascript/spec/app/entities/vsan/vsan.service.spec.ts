import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { VsanService } from 'app/entities/vsan/vsan.service';
import { IVsan, Vsan } from 'app/shared/model/vsan.model';

describe('Service Tests', () => {
  describe('Vsan Service', () => {
    let injector: TestBed;
    let service: VsanService;
    let httpMock: HttpTestingController;
    let elemDefault: IVsan;
    let expectedResult: IVsan | IVsan[] | boolean | null;
    let currentDate: moment.Moment;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(VsanService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new Vsan(0, 'AAAAAAA', 0, 0, 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', 0, 0, currentDate);
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            createDate: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a Vsan', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            createDate: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            createDate: currentDate,
          },
          returnedFromService
        );

        service.create(new Vsan()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a Vsan', () => {
        const returnedFromService = Object.assign(
          {
            filepath: 'BBBBBB',
            dir: 1,
            filesize: 1,
            status: 'BBBBBB',
            fileHash: 'BBBBBB',
            fileUuid: 'BBBBBB',
            vesUuid: 'BBBBBB',
            vv: 'BBBBBB',
            vesrr: 1,
            rr: 1,
            createDate: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            createDate: currentDate,
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of Vsan', () => {
        const returnedFromService = Object.assign(
          {
            filepath: 'BBBBBB',
            dir: 1,
            filesize: 1,
            status: 'BBBBBB',
            fileHash: 'BBBBBB',
            fileUuid: 'BBBBBB',
            vesUuid: 'BBBBBB',
            vv: 'BBBBBB',
            vesrr: 1,
            rr: 1,
            createDate: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            createDate: currentDate,
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Vsan', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
