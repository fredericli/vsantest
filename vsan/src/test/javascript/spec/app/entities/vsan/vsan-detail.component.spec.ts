import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { VsanTestModule } from '../../../test.module';
import { VsanDetailComponent } from 'app/entities/vsan/vsan-detail.component';
import { Vsan } from 'app/shared/model/vsan.model';

describe('Component Tests', () => {
  describe('Vsan Management Detail Component', () => {
    let comp: VsanDetailComponent;
    let fixture: ComponentFixture<VsanDetailComponent>;
    const route = ({ data: of({ vsan: new Vsan(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [VsanTestModule],
        declarations: [VsanDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(VsanDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(VsanDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load vsan on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.vsan).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
